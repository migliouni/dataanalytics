import csv

readFile = 'stops_for_graph_1.csv'
writeFile = 'aus.csv'

edges = {}

trips = []
#lettura del file contente i viaggi 'stagionali' da non considerare
with open('t1.csv') as csv_file:
    cvs_reader = csv.reader(csv_file, delimiter = ',')
    next(cvs_reader)

    for row in cvs_reader:
        if row[0] not in trips:
        	trips.append(row[0])

       	next(cvs_reader)

'''stops = {}
aus = 0

with open(readFile) as csv_file:
	cvs_reader = csv.reader(csv_file, delimiter = ',')
	next(cvs_reader)#salto intestazione

	for row in cvs_reader:
		key = str(row[1]) + "@" + str(row[2]) + "@" + str(row[3]) + "@" + str(row[4]) + "@" + str(row[5])
		#print(key)
		if key in stops.keys():
			print(key)
		else:
			stops[key] = row[0]
		aus += 1

print(aus)
print(len(stops))

csv_writer = csv.writer(open('aus.csv', 'w', newline=''), delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
csv_writer.writerow(['trip_id', 'stop_id', 'arrivo', 'partenza', 'route_id', 'name'])
for key in stops:
	row = []
	row.append(stops[key])
	row.extend(key.split('@'))

	csv_writer.writerow(row)'''

#----------------
#creazione degli archi del grafo dei treni partendo da l'elenco delle fermate estratte dal dataset,
#----------------

csv_writer = csv.writer(open('edges.csv', 'w', newline=''), delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

with open("stops_for_graph_1.csv") as csv_file:
	cvs_reader = csv.reader(csv_file, delimiter = ',')
	next(cvs_reader)#salto l'header

	prev = next(cvs_reader)

	for row in cvs_reader:
		if prev[0] == row[0] and row[0] not in trips:
			edge = str(prev[1]) + "@" + str(row[1]) + "@" + str(prev[4])#non considero i viaggi 'stagionali'
			if edge in edges.keys():
				edges[edge] += 1
			else:
				edges[edge] = 1

		prev = row

csv_writer.writerow(['node_1', 'node_2', 'line', 'trip_count'])
#creo il file degli archi del grafo delle linee
for edge in edges:
	row = edge.split('@')
	row.append(edges[edge])
	csv_writer.writerow(row)