import csv
from math import sin, cos, sqrt, atan2, radians

#converto l'orario in secondi
def stringToTime(string_time):
	string_split = string_time.split(":")
	time = int(string_split[0]) * 3600
	time += int(string_split[1]) * 60
	time += int(string_split[2])

	return time 

#calcola la distanza tra due posizioni geografice
def distance(lat1, lon1, lat2, lon2):
	R = 6373.0

	lat1 = radians(lat1)
	lon1 = radians(lon1)
	lat2 = radians(lat2)
	lon2 = radians(lon2)

	dlon = lon2 - lon1
	dlat = lat2 - lat1

	a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
	c = 2 * atan2(sqrt(a), sqrt(1 - a))

	distance = R * c

	return round(distance, 3)

trips = []
#lettura del file contente i viaggi 'stagionali' da non considerare
with open('t1.csv') as csv_file:
    cvs_reader = csv.reader(csv_file, delimiter = ',')
    next(cvs_reader)

    for row in cvs_reader:
        if row[0] not in trips:
        	trips.append(row[0])

       	next(cvs_reader)

readFile = 'stops_for_graph_2.csv'

#filtering
'''stops = {}
with open(readFile) as csv_file:
	cvs_reader = csv.reader(csv_file, delimiter = ',')
	next(cvs_reader)#salto intestazione

	for row in cvs_reader:
		key = str(row[1]) + "@" + str(row[2]) + "@" + str(row[3]) + "@" + str(row[4]) + "@" + str(row[5])
		stops[key] = str(row[0]) + "@" + str(row[5]) + "@" + str(row[6])

csv_writer = csv.writer(open('aus.csv', 'w', newline=''), delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
csv_writer.writerow(['trip_id', 'stop_id', 'arrivo', 'partenza', 'route_id', 'name', 'lat', 'lon'])
for key in stops:
	row = []
	values = stops[key].split("@")
	row.append(values[0])
	row.extend(key.split('@'))
	row.append(values[1])
	row.append(values[2])

	csv_writer.writerow(row)'''


#----------------
#creazione degli archi del grafo dei treni partendo da l'elenco delle fermate estratte dal dataset, 
#----------------

edges = {}

csv_writer = csv.writer(open('edges_time.csv', 'w', newline=''), delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
csv_writer.writerow(['node_1', 'node_2', 'line', 'arrivo', 'partenza', 'time', 'distance(km)', 'weight'])
with open(readFile) as csv_file:
	cvs_reader = csv.reader(csv_file, delimiter = ',')
	next(cvs_reader)

	prev = next(cvs_reader)

	for row in cvs_reader:
		#print(row)
		if prev[0] == row[0] and row[0] not in trips:#non considero i viaggi 'stagionali'
			start = stringToTime(prev[3])
			end = stringToTime(row[2])

			dist = distance(float(prev[5]), float(prev[6]), float(row[5]), float(row[6]))

			time_intervall = end - start

			csv_writer.writerow([prev[1], row[1], prev[4], prev[3], row[2], time_intervall, dist, int(time_intervall * dist)])

		prev = row