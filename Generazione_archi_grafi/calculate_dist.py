import csv
from math import sin, cos, sqrt, atan2, radians
#restituisce la distanza tra due punti geografici espressi in latitudine e longitudine
def distance(lat1, lon1, lat2, lon2):
	R = 6373.0

	lat1 = radians(lat1)
	lon1 = radians(lon1)
	lat2 = radians(lat2)
	lon2 = radians(lon2)

	dlon = lon2 - lon1
	dlat = lat2 - lat1

	a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
	c = 2 * atan2(sqrt(a), sqrt(1 - a))

	distance = R * c

	return round(distance, 3)

edges = {}

with open("stops_for_graph_2.csv") as csv_file:
	cvs_reader = csv.reader(csv_file, delimiter = ',')
	next(cvs_reader)

	prev = next(cvs_reader)

	for row in cvs_reader:
		edges[row[1]] = (row[5], row[6])

#creo file contenente la distanza tra ogni coppia di stazioni,
#usato per unire le stazioni che rappresentano lo stesso luogo fisico
csv_writer = csv.writer(open('distances.csv', 'w', newline=''), delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
csv_writer.writerow(['node_1', 'node_2', 'distance(km)'])

for node_1 in edges:
	for node_2 in edges:
		if node_1 != node_2:
			dist = distance(float(edges[node_1][0]), float(edges[node_1][1]), float(edges[node_2][0]), float(edges[node_2][1]))
			if dist <= 1:
				csv_writer.writerow([node_1, node_2, dist])


'''with open(readFile) as csv_file:
	cvs_reader = csv.reader(csv_file, delimiter = ',')
	next(cvs_reader)

	prev = next(cvs_reader)

	for row in cvs_reader:
		#print(row)
		if prev[0] == row[0]:
			start = stringToTime(prev[3])
			end = stringToTime(row[2])

			dist = distance(float(prev[5]), float(prev[6]), float(row[5]), float(row[6]))

			time_intervall = end - start

			csv_writer.writerow([prev[1], row[1], prev[4], prev[3], row[2], time_intervall, dist, int(time_intervall * dist)])

		prev = row'''