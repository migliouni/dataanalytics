import csv
import operator
import networkx as nx

#restituisce il nome di un nodo dato l'id e il grafo
def get_name(id, G):
	for node in list(G.nodes(data = True)):
		if node[0] == id: 
			return node[1]['labels']['label']

#restituisce l'id di un nodo dato il nome e il grafo
def get_id(name, G):
	for node in list(G.nodes(data = True)):
		if node[1]['labels']['label'] == name: 
			return node[0]

#restituisce un dizionario contenente il numero di treni totali per ogni linea
def treni_per_linea():
	readFile = "treni_per_linea.csv"
	trains = {}

	with open(readFile) as csv_file:
	    cvs_reader = csv.reader(csv_file, delimiter = ',')
	    next(cvs_reader)

	    for row in cvs_reader:
	        trains[row[1]] = row[0] 

	return trains

#restituisce l'elenco delle opzioni per gli elementi InputDropsown e l'elenco dei link relativi alla pagina wikidata di ogni stazione
#options = {'label': 'nome della stazione', 'value': id della stazione}
#links = {'nome della stazione': 'URI'}
def stations():
	options = []
	links = {}

	with open("nodes.csv") as csv_file:
	    cvs_reader = csv.reader(csv_file, delimiter = ',')
	    next(cvs_reader)

	    for row in cvs_reader:
	        options.append({'label': row[1], 'value': row[0]})
	        links[row[1]] = row[4]

	return (options, links)

#restuisce una lista di dizionari, dove ognuno di essi contiene l'elenco dei treni per ogni linea all'ora i-esima
def treni_per_linea_ora(threshold = 10):
	lines_counts = []
	lines = []
	with open("lines.csv") as csv_file:
		cvs_reader = csv.reader(csv_file, delimiter = ',')
		next(cvs_reader)

		for row in cvs_reader:
			lines.append((row[0], row[2]))

	for i in range(0, 26):
		lines_count = {}

		for line in lines:
			HH = int(line[1].split(":")[0])

			if HH >= i and HH < i+1:
				key = line[0]
				if key in lines_count:
					lines_count[key] += 1
				else:
					lines_count[key] = 1

		aus = {}
		sorted_d = sorted(lines_count.items(), key=operator.itemgetter(1), reverse=True)

		#get firts 20 elements
		if len(lines_count) > threshold:
			i = 0
			for i in range(0, threshold):
				aus[sorted_d[i][0]] = sorted_d[i][1]

			lines_counts.append(aus)
		else:
			i = 0
			for i in range(0, len(sorted_d)):
				aus[sorted_d[i][0]] = sorted_d[i][1]

			lines_counts.append(aus)

	'''line_aus = []
	line_count_f = []

	for hh in lines_counts:
		line_aus = []
		for el in hh:
			line_aus.append(el)

		cnt = {}
		for el in line_aus:
			if el in cnt:
				cnt[el] += 1
			else: cnt[el] = 1

		line_count_f.append(cnt)'''

	return lines_counts

#restituisce una lista di dizionari dove ognuno di essi in posizione i-esima contiene il numero di treni all'ora i-esima
def treni_per_ora(threshold = 10):
	stops = []
	lines = []
	stops_counts = []
	stops_counts_with_platform = []
	lines_counts = []
	dictNodes = {}

	readFile = 'stops.csv'

	with open(readFile) as csv_file:
	    cvs_reader = csv.reader(csv_file, delimiter = ',')
	    next(cvs_reader)

	    for row in cvs_reader:
	        stops.append((row[0], row[1]))

	readFileNodes = 'nodes.csv'     
	with open(readFileNodes) as csv_file:
	    cvs_reader = csv.reader(csv_file, delimiter = ',')
	    next(cvs_reader)

	    for row in cvs_reader:
	        dictNodes[row[1]] =  row[5]

	for i in range(0, 26):
	    stops_count = {}

	    for stop in stops:
	        HH = int(stop[1].split(":")[0])

	        if HH >= i and HH < i+1:
	            if stop[0] in stops_count:
	                stops_count[stop[0]] += 1
	            else:
	                stops_count[stop[0]] = 1

	    aus = {}
	    aus_with_platform = {}
	    stops_count_with_platform = stops_count.copy()
	    for station, trainCounts in stops_count_with_platform.items():
	        if station in dictNodes:
	            stops_count_with_platform[station] = trainCounts/ int(dictNodes[station])
	        
	    
	    sorted_d = sorted(stops_count.items(), key=operator.itemgetter(1), reverse=True)
	    sorted_d_with_platform = sorted(stops_count_with_platform.items(), key=operator.itemgetter(1), reverse=True)

	    #get firts 20 elements
	    if len(stops_count) > threshold:
	        i = 0
	        for i in range(0, threshold):
	            aus[sorted_d[i][0]] = sorted_d[i][1]

	        stops_counts.append(aus)
	    else:
	        i = 0
	        for i in range(0, len(sorted_d)):
	            aus[sorted_d[i][0]] = sorted_d[i][1]

	        stops_counts.append(aus)
	    
	    
	    #get firts 20 elements considering platform
	    if len(stops_count_with_platform) > threshold:
	        i = 0
	        for i in range(0, threshold):
	            aus_with_platform[sorted_d_with_platform[i][0]] = sorted_d_with_platform[i][1]

	        stops_counts_with_platform.append(aus_with_platform)
	    else:
	        i = 0
	        for i in range(0, len(sorted_d_with_platform)):
	            aus_with_platform[sorted_d_with_platform[i][0]] = sorted_d_with_platform[i][1]

	        stops_counts_with_platform.append(aus_with_platform)

	return (stops_counts, stops_counts_with_platform)

#restituisce l'elenco delle stazioni ordinate in base al numero totali di treni passanti nella giornata, 
#threshold indica il numero di elementi da restituire
def top_stazioni(stops_counts, threshold = 5):
	top = {}

	for hour in stops_counts:
		for stop in hour:
			if stop in top.keys():
				top[stop] += hour[stop]
			else:
				top[stop] = hour[stop]

	top = sorted(top.items(), key=operator.itemgetter(1), reverse=True)

	return top[:threshold]

#restituisce i primi 10 nodi con degree maggiore dato il grafo
def get_top_degree(M):
	degree = M.degree(weight = 'trip_count')

	sorted_degree = sorted(degree, key=lambda tup: tup[1], reverse = True)

	nodes = [{'label': get_name(n[0], M), 'value': n[0]} for n in sorted_degree]

	return nodes[:10]

#restituisce i primi 10 nodi con betweenness maggiore dato il grafo
def get_top_bet(M):
	betweenness_centrality = nx.betweenness_centrality(M)

	sorted_bet = sorted(betweenness_centrality.items(), key=operator.itemgetter(1), reverse = True)

	nodes = [{'label': get_name(n[0], M), 'value': n[0]} for n in sorted_bet]

	return nodes[:10]

#restituisce il grafo in forma di figura dopo aver eliminato i nodi dell'elenco nodes_rm,
#ad ogni componente connesse viene associato un colore differente
def graph_rm_nodes(nodes_rm, M):
	
	color = ['#3498db', '#f1c40f', '#e74c3c', '#1abc9c', '#2c3e50']
	if nodes_rm is not None:
		M.remove_nodes_from(nodes_rm)
	#components = list(nx.weakly_connected_component_subgraphs(M))

	components = list(M.subgraph(c) for c in nx.weakly_connected_components(M))

	dicto = M.nodes(data = True)

	dicto = sorted(dicto, key=lambda tup: int(tup[0]), reverse = False)

	pos = {}
	#for node in dicto:
		#pos[node] = 
	labels = []
	for label in dicto:
		labels.append(label[1]['labels']['label'])
		pos[label[0]] = label[1]['labels']['pos']

	node_colors = []
	sizes = []
	sorted_nodes = sorted(M.nodes(), key=lambda x: int(x), reverse = False)

	for node in sorted_nodes:
		for i in range(0, len(components)):
			if node in components[i].nodes():
				node_colors.append(color[i])

	Xn=[pos[k][0] for k in pos]
	Yn=[pos[k][1] for k in pos]
	trace_nodes=dict(type='scatter',
	                 x=Xn, 
	                 y=Yn,
	                 mode='markers',
	                 marker=dict(size=10, color= node_colors),
	                 opacity = 0.9,
	                 text=labels,
	                 hoverinfo='text')

	Xe=[]
	Ye=[]
	for e in M.edges():
	    Xe.extend([pos[e[0]][0], pos[e[1]][0], None])
	    Ye.extend([pos[e[0]][1], pos[e[1]][1], None])

	trace_edges=dict(type='scatter',
	                 mode='lines',
	                 x=Xe,
	                 y=Ye,
	                 line=dict(width=1, color='rgb(25,25,25)'),
	                 hoverinfo='none' 
	                )

	axis=dict(showline=False, # hide axis line, grid, ticklabels and  title
	          zeroline=False,
	          showgrid=False,
	          showticklabels=False,
	          title='' 
	          )
	layout=dict(title= 'Componenti',  
	            font= dict(family='Balto'),
	            width=800,
	            height=800,
	            autosize=False,
	            showlegend=False,
	            xaxis=axis,
	            yaxis=axis,
	            margin=dict(
	            l=40,
	            r=40,
	            b=85,
	            t=100,
	            pad=0,
	       
	    ),
	    hovermode='closest',
	    plot_bgcolor='#efecea', #set background color            
	    )

	fig = dict(data=[trace_edges, trace_nodes], layout=layout)

	return fig

















