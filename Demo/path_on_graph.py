import networkx as nx
import csv
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import operator
import random
from collections import deque

#crea la figura del grafo con evidenziati i nodi del percorso,
#il percorso viene recuperato dal file path.txt
#ad ogni linea viene associato un colore differente
def get_path_fig():
	fileName = 'edges_g1.csv'

	M = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = [('line', str), ('trip_count', int)])

	dicto = {}
	dict_pos = {}
	with open('nodes.csv') as csv_file:
		cvs_reader = csv.reader(csv_file, delimiter = ',')
		next(cvs_reader)

		for row in cvs_reader:
			dicto[row[0]] = {'label' : row[1]}
			dict_pos[row[0]] = (float(row[3]), float(row[2]))		

	nx.set_node_attributes(M, dicto, 'labels')

	with open('path.txt', 'r') as f:
		s = f.read()
		path = eval(s)

	dict_colors = {'R1': '#1FCA1F', 'R11': '#352812', 'R12': '#3647DD', 'R13': '#6E4A65', 'R14': '#D62011', 'R16': '#3EE176', 'R17': '#0047B7', 'R18': '#BF5EB1', 'R2': '#F5130C', 'R21': '#D653B4', 'R22': '#914ED3', 'R23': '#A71567', 'R25': '#C186D0', 'R27': '#50AAE2', 'R28': '#C1FBE5', 'R3': '#044872', 'R31': '#30F120', 'R32': '#60E42E', 'R34': '#824AA2', 'R35': '#10B0A2', 'R36': '#26DB0F', 'R37': '#A0001E', 'R38': '#C368EA', 'R39': '#D49545', 'R4': '#DDE94C', 'R40': '#DA47C1', 'R41': '#CEDA05', 'R5': '#A7B525', 'R6': '#B059BA', 'R7': '#9FB970', 'R8': '#B56FF5', 'RE_1': '#8D9BBC', 'RE_10': '#A0AC2B', 'RE_11': '#EE5C17', 'RE_13': '#FF64F5', 'RE_2': '#DDF3DD', 'RE_3': '#797AA8', 'RE_4': '#D3504E', 'RE_5': '#BF587D', 'RE_6': '#627CF4', 'RE_7': '#3CB7B5', 'RE_8': '#BAF798', 'S1': '#E4A098', 'S10': '#825F5D', 'S11': '#6D52B8', 'S12': '#39A5B8', 'S13': '#FAB705', 'S2': '#992B90', 'S3': '#CBBC48', 'S4': '#C5E07D', 'S40': '#4D4758', 'S5': '#B108A5', 'S50': '#07DAEB', 'S6': '#9D85B4', 'S7': '#004C84', 'S8': '#6F3BFB', 'S9': '#9602B3', 'XP1': '#D223EC', 'XP2': '#972CAB', 'PIEDI': '#D6D6D6'}

	sorted_nodes = sorted(M.nodes(), key=lambda x: int(x), reverse = False)

	pos = dict_pos
	labels = []
	for label in dicto:
	    labels.append(dicto[label]['label'])

	node_colors = []
	sizes = []
	for node in sorted_nodes:
		if node in path.keys():
			sizes.append(12)
			if path[node] is not None:
				node_colors.append(dict_colors[path[node]])
			else:
				node_colors.append('#ffffff')
		else:
			sizes.append(4)
			node_colors.append('#000000')

	Xn=[pos[k][0] for k in pos]
	Yn=[pos[k][1] for k in pos]
	trace_nodes=dict(type='scatter',
	                 x=Xn, 
	                 y=Yn,
	                 mode='markers',
	                 marker=dict(size=sizes, color= node_colors),
	                 opacity = 0.9,
	                 text=labels,
	                 hoverinfo='text')

	Xe=[]
	Ye=[]
	for e in M.edges():
	    Xe.extend([pos[e[0]][0], pos[e[1]][0], None])
	    Ye.extend([pos[e[0]][1], pos[e[1]][1], None])

	trace_edges=dict(type='scatter',
	                 mode='lines',
	                 x=Xe,
	                 y=Ye,
	                 line=dict(width=1, color='rgb(25,25,25)'),
	                 hoverinfo='none' 
	                )

	axis=dict(showline=False, # hide axis line, grid, ticklabels and  title
	          zeroline=False,
	          showgrid=False,
	          showticklabels=False,
	          title='' 
	          )
	layout=dict(title= 'Percorso',  
	            font= dict(family='Balto'),
	            width=800,
	            height=800,
	            autosize=False,
	            showlegend=False,
	            xaxis=axis,
	            yaxis=axis,
	            margin=dict(
	            l=40,
	            r=40,
	            b=85,
	            t=100,
	            pad=0,
	       
	    ),
	    hovermode='closest',
	    plot_bgcolor='#efecea', #set background color            
	    )

	fig = dict(data=[trace_edges, trace_nodes], layout=layout)

	return fig