import mydijkstra as md

import networkx as nx
import csv
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import operator
import random
from collections import deque
import sys

#restituisce il nome di un nodo dato l'id e il grafo G
def get_name(id, G):
	for node in list(G.nodes(data = True)):
		if node[0] == id: 
			return node[1]['labels']['label']

#restituisce l'id di una stazione dato il nome e il grafo G
def get_id(name, G):
	for node in list(G.nodes(data = True)):
		if node[1]['labels']['label'] == name: 
			return node[0]

#si calcola il percorso tramite gli argomenti passati, 
#successivamente viene generato un immagine e un file di testo con il percorso,
#ad ogni linea viene associato un colore differente
if len(sys.argv) == 4:
	id_source = sys.argv[1]#id del nodo sorgente
	#id_source = get_id(sys.argv[1], M)
	id_dest = sys.argv[2]#id del nodo di destinazione
	#id_dest = get_id(sys.argv[2], M)
	start_time = sys.argv[3]#orario di partenza

	fileName = 'edges_add_edge.csv'

	data = [('line', str), ('partenza', str), ('arrivo', str), ('tempo', int), ('distanza', float), ('weight', float), ('trip_id', str)]

	M = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = data)

	dicto = {}

	with open('nodes.csv') as csv_file:
		cvs_reader = csv.reader(csv_file, delimiter = ',')
		next(cvs_reader)

		for row in cvs_reader:
			dicto[row[0]] = {'label' : row[1]}

	M.add_nodes_from(list(dicto.keys()))

	nx.set_node_attributes(M, dicto, 'labels')

	path, distances = md.mydijkstra(M, id_source, start_time, id_dest)

	string_path = ''
	f = open("path.txt", "w")

	#f.write(str(path))

	edges = []

	for i in range(0, len(path) -1, 1):
	    edge_key = path[i+1][1]
	    string_path += str((get_name(path[i][0], M), nx.get_edge_attributes(M, 'partenza')[edge_key], nx.get_edge_attributes(M, 'arrivo')[edge_key], get_name(path[i+1][0], M), nx.get_edge_attributes(M, 'line')[edge_key]))
	    string_path += "\n"
	    edges.append((path[i][0], path[i+1][0], {'partenza': nx.get_edge_attributes(M, 'partenza')[edge_key], 'arrivo': nx.get_edge_attributes(M, 'arrivo')[edge_key], 'linea': nx.get_edge_attributes(M, 'line')[edge_key]}))

	#f.write(string_path)
	f.close

	G_p = nx.Graph()
	for edge in edges:	
		G_p.add_edge(edge[0], edge[1], partenza = edge[2]['partenza'], arrivo = edge[2]['arrivo'], linea = edge[2]['linea'])

	dicto = {}

	for node in list(G_p.nodes()):
		dicto[node] = {'label' : get_name(node, M)}

	M.add_nodes_from(list(dicto.keys()))

	nx.set_node_attributes(G_p, dicto, 'labels')

	pos = {}
	label_pos = {}
	labels = {}
	edge_label_pos = {}
	i = 0
	x = 0
	for node in list(G_p.nodes(data = True)):
		labels[node[0]] = node[1]['labels']['label']
		pos[node[0]] = [x, i]
		label_pos[node[0]] = [x - 0.003, i]
		edge_label_pos[node[0]] = [x + 0.002, i]
		i -= 1

	edge_labels = {}
	for edge in list(G_p.edges(data = True)):
		key = (edge[0], edge[1])
		if edge[2]['partenza'] != "XX:XX:XX":
			edge_labels[key] = edge[2]['partenza'] + ", " + edge[2]['arrivo']

	nodes_lines = {}
	lines_color = {}
	dict_colors = {'R11': '#352812', 'R12': '#3647DD', 'R13': '#6E4A65', 'R14': '#D62011', 'R16': '#3EE176', 'R17': '#0047B7', 'R18': '#BF5EB1', 'R2': '#F5130C', 'R21': '#D653B4', 'R22': '#914ED3', 'R23': '#A71567', 'R25': '#C186D0', 'R27': '#50AAE2', 'R28': '#C1FBE5', 'R3': '#044872', 'R31': '#30F120', 'R32': '#60E42E', 'R34': '#824AA2', 'R35': '#10B0A2', 'R36': '#26DB0F', 'R37': '#A0001E', 'R38': '#C368EA', 'R39': '#D49545', 'R4': '#DDE94C', 'R40': '#DA47C1', 'R41': '#CEDA05', 'R5': '#A7B525', 'R6': '#B059BA', 'R7': '#9FB970', 'R8': '#B56FF5', 'RE_1': '#8D9BBC', 'RE_10': '#A0AC2B', 'RE_11': '#EE5C17', 'RE_13': '#FF64F5', 'RE_2': '#DDF3DD', 'RE_3': '#797AA8', 'RE_4': '#D3504E', 'RE_5': '#BF587D', 'RE_6': '#627CF4', 'RE_7': '#3CB7B5', 'RE_8': '#BAF798', 'S1': '#E4A098', 'S10': '#825F5D', 'S11': '#6D52B8', 'S12': '#39A5B8', 'S13': '#FAB705', 'S2': '#992B90', 'S3': '#CBBC48', 'S4': '#C5E07D', 'S40': '#4D4758', 'S5': '#B108A5', 'S50': '#07DAEB', 'S6': '#9D85B4', 'S7': '#004C84', 'S8': '#6F3BFB', 'S9': '#9602B3', 'XP1': '#D223EC', 'XP2': '#972CAB', 'PIEDI': '#d6d6d6'}

	colors = ['red', 'green', 'blue', 'yellow', 'orange']

	first = list(G_p.edges(data = True))[0][0]
	linea = list(G_p.edges(data = True))[0][2]['linea']

	if linea not in nodes_lines:
		nodes_lines[linea] = deque()
		lines_color[linea] =  dict_colors[linea]
	nodes_lines[linea].appendleft(first)

	for edge in list(G_p.edges(data = True)):
		linea = edge[2]['linea']
		if linea not in nodes_lines:
			nodes_lines[linea] = deque()
			lines_color[linea] =  dict_colors[linea]
		nodes_lines[linea].append(edge[1])

	#pos = nx.spring_layout(G_p)

	height = G_p.number_of_nodes() / 2

	if height < 6: height = 6

	plt.figure(figsize = (8, height))

	for linea in nodes_lines:
		nodes = nodes_lines[linea]
		nx.draw_networkx_nodes(G_p, pos = pos, nodelist = nodes, node_color = lines_color[linea], node_size = 50)

	nx.draw_networkx_edges(G_p, pos = pos)
	nx.draw_networkx_labels(G_p, pos = label_pos, labels = labels, font_size = 8)
	e_label_draw = nx.draw_networkx_edge_labels(G_p, pos = edge_label_pos, edge_labels = edge_labels, font_size = 8)

	for _,t in e_label_draw.items():
		t.set_rotation('horizontal')

	patchs = []
	for line in lines_color:
		patchs.append(mpatches.Patch(color = lines_color[line], label = line))

	plt.draw()  # pyplot draw()
	plt.legend(handles = patchs)
	plt.savefig("path.png")

	all_lines = nx.get_edge_attributes(M, 'line')

	file_data = []
	print(all_lines)

	path_nodes = {}
	for edge in path:
		if edge[1] is not None:
			path_nodes[edge[0]] = all_lines[edge[1]]
		else:
			path_nodes[edge[0]] = None

	f.write(str(path_nodes))
	#percorso = {}

	#for i in range(1, len(path), 1):
		#percorso.append((path[i][1], all_lines[path[i][1]]))
		#percorso[path[i][1]] = all_lines[path[i][1]]

	#return get_path_fig(percorso)


