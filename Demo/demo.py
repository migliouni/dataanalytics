# -*- coding: utf-8 -*-
import csv
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

import networkx as nx
import platform
import plotly
from plotly.offline import download_plotlyjs, init_notebook_mode,  iplot, plot
init_notebook_mode(connected=True)
import pexpect

import base64

#import path_on_file as pof
#import prova_short_percorsi as p_graph
import functions as fc
import plot_lines_of as plo
import plot_cent as pc
import path_on_graph as pog

import os

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

stops_counts, stops_counts_with_platform = fc.treni_per_ora()

line_count_f = fc.treni_per_linea_ora()

trains = fc.treni_per_linea()

options, links = fc.stations()

top = fc.top_stazioni(stops_counts)

top_pl = fc.top_stazioni(stops_counts_with_platform)

station = ''

#creating graph
fileName = 'edges_add_edge.csv'

data = [('line', str), ('partenza', str), ('arrivo', str), ('tempo', int), ('distanza', float), ('weight', float), ('trip_id', str)]

M = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = data)

dicto = {}
dict_pos = {}

with open('nodes.csv') as csv_file:
    cvs_reader = csv.reader(csv_file, delimiter = ',')
    next(cvs_reader)

    for row in cvs_reader:
        dicto[row[0]] = {'label' : row[1]}
        dict_pos[row[0]] = (float(row[3]), float(row[2]))   

M.add_nodes_from(list(dicto.keys()))

nx.set_node_attributes(M, dicto, 'labels')

fileName = 'edges_g1.csv'

G = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = [('line', str), ('trip_count', int)])

dicto = {}
dict_pos = {}
with open('nodes.csv') as csv_file:
    cvs_reader = csv.reader(csv_file, delimiter = ',')
    next(cvs_reader)

    for row in cvs_reader:
        dicto[row[0]] = {'label' : row[1], 'pos': (row[3], row[2])}
        dict_pos[row[0]] = (float(row[3]), float(row[2]))

nx.set_node_attributes(G, dicto, 'labels')

sorted_degree = fc.get_top_degree(G)
sorted_degree.append({'label': 'Tutti', 'value': '0'})

sorted_bet = fc.get_top_bet(G)
sorted_bet.append({'label': 'Tutti', 'value': '0'})

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.title = 'Trenord'

app.layout = html.Div(children=[
    html.H1(children='Trenord'),
    dcc.Tabs(id = 'tabs', children = [
        dcc.Tab(label = 'Misure di centralità', children = [
            html.Div([
                dcc.Dropdown(
                    id = 'centrality',
                    options = [{'label': 'Degree', 'value': 0}, {'label': 'Closeness', 'value': 1}, {'label': 'Beetweenness', 'value': 2}],
                    value = 0,
                    style = {'width' : '100%'}
                ),
                dcc.Input(id='input-links', type='hidden', value = str(links)),
                html.A(
                    'Prova',
                    id = 'station-link',
                    target="_blank",
                    style = {'display': 'inline-block', 'width' : '15%', 'float': 'left', 'margin-top': '100px'}
                ),
                dcc.Graph(
                    id = 'graph-cent',
                    style = {'display': 'inline-block', 'width' : '70%'}
                )],
                style = {'padding' : '50px'}
            )
        ]),
        dcc.Tab(label = 'Simulazione attacco', children = [
            html.Div([
                dcc.RadioItems(
                    id = 'cent-select',
                    options=[
                        {'label': 'Degree', 'value': '0'},
                        {'label': 'Beetweenness', 'value': '1'}
                    ],
                    value='0',
                    style = {'display': 'inline-block', 'width': '20%', 'float': 'left'}
                ), 
                dcc.Dropdown(
                    id = 'cent-top-nodes',
                    multi = True,
                    style = {'display': 'inline-block', 'width': '80%'}
                ),
                dcc.Graph(
                    id = 'graph_cc',
                    style = {'margin-left': '20%'}
                )
            ],
            style = {'padding': '50px'})
        ]),
        dcc.Tab(label = 'Congestione delle stazioni', children = [
            html.Div([
                html.Div(
                    dcc.Graph(
                        id='stops--hour---graph',
                        figure={
                            'data': [
                                {'x': list(stops_counts[13].keys()), 'y': list(stops_counts[13].values()), 'type': 'bar', 'name': 'SF'},
                            ],
                            'layout': {
                                'title': 'Tratte per fermata',
                                'font': {
                                    'size': 5
                                }
                            }
                        }
                    )
                ),

                html.Div(
                    dcc.Slider(
                        id='hour--slider--stops',
                        min=4,
                        max=25,
                        value=13,
                        marks={str(i): str(i) for i in range(4,26)},
                        step=None
                    ),
                    style = {'width': '80%', 'margin-left': '10%'}
                )],
                style = {'width': '70%', 'display': 'inline-block'}
            ),

            html.Div([
                html.H4('Stazioni più congestionate'),
                html.Ul([html.Li(html.A(str(x[0]), href = links[x[0]], target="_blank")) for x in top])],
                style = {'width': '20%', 'padding-left': '50px', 'display': 'inline-block', 'margin-top': '-100px'}
            ),

            html.Div([
                html.Div(
                    dcc.Graph(
                        id='stops--hourplatform---graph',
                        figure={
                            'data': [
                                {'x': list(stops_counts_with_platform[13].keys()), 'y': list(stops_counts_with_platform[13].values()), 'type': 'bar', 'name': 'SF'},
                            ],
                            'layout': {
                                'title': 'Tratte per fermata',
                                'font': {
                                    'size': 5
                                }
                            }
                        }
                    )
                ),

                html.Div(
                    dcc.Slider(
                        id='hourplatform--slider--stops',
                        min=4,
                        max=25,
                        value=13,
                        marks={str(i): str(i) for i in range(4,26)},
                        step=None
                    ),
                    style = {'width': '80%', 'margin-left': '10%'}
                )],
                style = {'width': '70%', 'display': 'inline-block', 'margin-top': '50px'}
            ),

            html.Div([
                html.H4('Stazioni più congestionate'),
                html.Ul([html.Li(html.A(str(x[0]), href = links[x[0]], target="_blank")) for x in top_pl])],
                style = {'width': '20%', 'padding-left': '50px', 'display': 'inline-block', 'margin-top': '-100px'}
            ),

            html.Div([
                html.Div([
                    html.P('Stazioni'),
                    dcc.Dropdown(
                       id = 'station-plot-line',
                       options = options,
                       value = '1917'
                    )
                ],
                    style = {'display': 'inline-block', 'width': '20%', 'float': 'left', 'margin-top': '100px'}
                ),
                dcc.Graph(
                    id="line-of-graph",
                    style = {'display': 'inline-block', 'width': '20%'}
                ),
                html.Div(
                    id = 'lines-of-div'
                )],
                style = {'margin-top': '50px', 'padding': '50px'}
            )
        ]),
        dcc.Tab(label = 'Congestione delle linee', children = [
            html.Div([
                html.Div(
                    dcc.Graph(
                        id='line--hour--graph',
                        figure={
                            'data': [
                                {'x': list(line_count_f[13].keys()), 'y': list(line_count_f[13].values()), 'type': 'bar', 'name': 'SF'},
                            ],
                            'layout': {
                                'title': 'Treni per linea all\'ora',
                                'font': {
                                    'size': 10
                                }
                            }
                        }
                    )
                ),

                html.Div(
                    dcc.Slider(
                        id='hour--slider--lines',
                        min=4,
                        max=25,
                        value=13,
                        marks={str(i): str(i) for i in range(4,26)},
                        step=None
                    ),
                    style = {'width': '80%', 'margin-left': '10%'}
                )],
                style = {'width': '100%', 'display': 'inline-block'}
            ),

            html.Div(
                dcc.Graph(
                    id='train-count',
                    figure={
                        'data': [
                            {'x': list(trains.keys()), 'y': list(trains.values()), 'type': 'bar', 'name': 'SF'},
                        ],
                        'layout': {
                            'title': 'Treni totali che partono per ogni linea durante una giornata',
                            'font': {
                                'size': 10
                            }
                        }
                    }
                )
            )
        ]),
        dcc.Tab(label = 'Ricerca percorso', children = [
            html.Div([
                html.Div([
                    html.P('Stazione di partenza'),
                    dcc.Dropdown(
                       id = 'stop-search',
                       options = options,
                       value = '1917'
                    )],
                    style = {'margin-right' : '10px'}
                ),
                html.Div([
                    html.P('Stazione di arrivo'),
                     dcc.Dropdown(
                         id = 'dest-search',
                         options = options,
                         value = '1550'
                    )],
                    style = {'margin-right' : '10px'}
                ),
                html.Div([
                    html.P('Orario di partenza'),
                    dcc.Input(
                         type='text',
                         id = 'time-search',
                         value = '07:00:00'
                    )],
                    style = {'margin-right' : '10px'}
                ),
                html.Div([
                    html.P('pippo', style = {'color': 'white'}),
                    html.Button(
                        'Cerca', 
                        id='path_submit'
                    )],
                    style = {'margin-right': '10px'}
                )],
                style = {'display': 'grid', 'grid-template-columns': '1fr 1fr 0.5fr 0.5fr', 'padding': '70px'}
            ),

            html.Div(
                dcc.Loading(
                    id="loading-2",
                    children=[
                        html.Img(
                            id="body-image",
                            style = {'width': '45%', 'display': 'inline-block'}
                        ),
                        dcc.Graph(
                            id = 'path-graph',
                            style = {'width': '40%', 'display': 'inline-block'}
                        )
                    ],
                    type="dot",
                    style = {'margin-top': '50px'}
                ),
                style = {'width': '100%'}
            ),

            html.Div(
                style = {'height': 400}
            )
        ])
    ])
])

@app.callback(
    dash.dependencies.Output('graph-cent', 'figure'),
    [dash.dependencies.Input('centrality', 'value')])
def update_cent(value):
    return pc.get_cent(value)

@app.callback(
    Output('stops--hour---graph', 'figure'),
    [Input('hour--slider--stops', 'value')])
def update_graph(hour_value):

    return {
        'data': [
                {'x': list(stops_counts[hour_value].keys()), 'y': list(stops_counts[hour_value].values()), 'type': 'bar', 'name': 'SF'},
            ],
        'layout': {
            'title': 'Congestione totale delle stazioni per ogni ora della giornata',
            'font': {
                'size': 10
            }
        }
    }

@app.callback(
    Output('line--hour--graph', 'figure'),
    [Input('hour--slider--lines', 'value')])
def update_graph(hour_value):

    return {
        'data': [
                {'x': list(line_count_f[hour_value].keys()), 'y': list(line_count_f[hour_value].values()), 'type': 'bar', 'name': 'SF'},
            ],
        'layout': {
            'title': 'Numero di treni che partono per ogni linea in ogni ora della giornata',
            'font': {
                'size': 10
            }
        }
    }

@app.callback(
    Output('stops--hourplatform---graph', 'figure'),
    [Input('hourplatform--slider--stops', 'value')])
def update_graph(hour_value):

    return {
        'data': [
                {'x': list(stops_counts_with_platform[hour_value].keys()), 'y': list(stops_counts_with_platform[hour_value].values()), 'type': 'bar', 'name': 'SF'},
            ],
        'layout': {
            'title': 'COngestione considerando il numero di binari della stazione per ora della giornata',
            'font': {
                'size': 10
            }
        }
    }
@app.callback(
    dash.dependencies.Output("body-image", "src"),
    [dash.dependencies.Input('path_submit', 'n_clicks')],
    [dash.dependencies.State('stop-search', 'value'),
    dash.dependencies.State('dest-search', 'value'),
    dash.dependencies.State('time-search', 'value')
    ])
def update_output(n_clicks, start, dest, start_time):
    id_source = format(start)
    id_dest = format(dest)
    start_time = format(start_time)
    image_path = "path.png"

    command = "python3 path_on_file.py " + id_source + " " + id_dest +" '" + start_time + "'"

    print(command)

    pexpect.run(command)

    encoded_image = base64.b64encode(open(image_path, 'rb').read())
    return 'data:image/png;base64,{}'.format(encoded_image.decode())

@app.callback(
    dash.dependencies.Output("path-graph", "figure"),
    [dash.dependencies.Input('body-image', 'src')])
def update_output(n_clicks):
    print('Loading path on network')

    return pog.get_path_fig()

@app.callback(
    dash.dependencies.Output("line-of-graph", "figure"),
    [dash.dependencies.Input('station-plot-line', 'value')])
def update_line(node_id):
    fig = plo.get_fig(node_id)

    return fig    

@app.callback(
    dash.dependencies.Output('station-link', 'href'),
    [dash.dependencies.Input('graph-cent', 'clickData')])
def get_URI(clickData):
    global station
    global links
    uri = 'none'
    station = ''
    if clickData is not None:
        station = clickData['points'][0]['text']
        uri = links[station]

    return(uri)

@app.callback(
    dash.dependencies.Output('station-link', 'children'),
    [dash.dependencies.Input('station-link', 'href')])
def update_link_name(href):
    return station

@app.callback(
    dash.dependencies.Output('cent-top-nodes', 'options'),
    [dash.dependencies.Input('cent-select', 'value')])
def update_top_nodes(value):
    global sorted_degree
    global sorted_bet
    if value == '0': 
        return sorted_degree
    else: 
        return sorted_bet

@app.callback(
    dash.dependencies.Output('graph_cc', 'figure'),
    [dash.dependencies.Input('cent-top-nodes', 'value')],
    [dash.dependencies.State('cent-top-nodes', 'options')])
def updat_graph_cc(value, options):
    if value is not None:
        if '0' in value:
            value = []
            value = [o['value'] for o in options]

    global G
    global dict_pos
    fig = fc.graph_rm_nodes(value, G.copy())
    return fig

if __name__ == '__main__':
    app.run_server(debug = True)