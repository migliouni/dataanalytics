import networkx as nx
import csv
import matplotlib.pyplot as plt
import operator
import numpy as np

#restituisce il nome di una stazione dato il suo id e il grafo G
def get_name(id, G):
	for node in list(G.nodes(data = True)):
		if node[0] == id: 
			return node[1]['labels']['label']
#restituisce una figura contenente il grafo delle linee
#la dimensione dei nodi è maggiore più è centrale, in base alla centralità cent_id
#cent_id = 0 -> degree
#cent_id = 1 -> closeness
#cent_id = 2 -> betweenness
def get_cent(cent_id):
	fileName = 'edges_g1.csv'

	M = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = [('line', str), ('trip_count', int)])

	dicto = {}
	dict_pos = {}
	with open('nodes.csv') as csv_file:
		cvs_reader = csv.reader(csv_file, delimiter = ',')
		next(cvs_reader)

		for row in cvs_reader:
			dicto[row[0]] = {'label' : row[1]}
			dict_pos[row[0]] = (float(row[3]), float(row[2]))	

	pos = dict_pos
	labels = []
	for label in dicto:
	    labels.append(dicto[label]['label'])

	node_colors = []
	sizes = []
	nodes = {}

	cent = None

	if cent_id == 0:
		alpha = 1/25
		degree = M.degree(weight = 'trip_count')
		cent = sorted(degree, key=lambda tup: int(tup[0]), reverse = False)
		#print(cent)
	if cent_id == 1:
		alpha = 100
		closeness_centrality = nx.closeness_centrality(M)
		cent = sorted(closeness_centrality.items(), key=lambda tup: int(tup[0]), reverse = False)
		#print(cent)
	if cent_id == 2:
		alpha = 100
		betweenness_centrality = nx.betweenness_centrality(M)
		print(betweenness_centrality)
		cent = sorted(betweenness_centrality.items(), key=lambda tup: int(tup[0]), reverse = False)

	for node in cent:
		sizes.append(node[1]*alpha)

	Xn=[pos[k][0] for k in pos]
	Yn=[pos[k][1] for k in pos]
	trace_nodes=dict(type='scatter',
	                 x=Xn, 
	                 y=Yn,
	                 mode='markers',
	                 marker=dict(size=sizes, color= '#1FCA1F'),
	                 opacity = 0.9,
	                 text=labels,
	                 hoverinfo='text')

	Xe=[]
	Ye=[]
	for e in M.edges():
	    Xe.extend([pos[e[0]][0], pos[e[1]][0], None])
	    Ye.extend([pos[e[0]][1], pos[e[1]][1], None])

	trace_edges=dict(type='scatter',
	                 mode='lines',
	                 x=Xe,
	                 y=Ye,
	                 line=dict(width=1, color='rgb(25,25,25)'),
	                 hoverinfo='none' 
	                )

	axis=dict(showline=False, # hide axis line, grid, ticklabels and  title
	          zeroline=False,
	          showgrid=False,
	          showticklabels=False,
	          title='' 
	          )
	layout=dict(title= '',  
	            font= dict(family='Balto'),
	            width=1000,
	            height=1000,
	            autosize=False,
	            showlegend=False,
	            xaxis=axis,
	            yaxis=axis,
	            margin=dict(
	            l=40,
	            r=40,
	            b=85,
	            t=100,
	            pad=0,
	       
	    ),
	    hovermode='closest',
	    plot_bgcolor='#efecea', #set background color            
	    )

	fig = dict(data=[trace_edges, trace_nodes], layout=layout)

	return fig