####Analisi della resistenza della rete ad attacchi####


import networkx as nx
import csv
import matplotlib.pyplot as plt
import operator
import numpy as np
import random

def get_name(id, G):
	for node in list(G.nodes(data = True)):
		if node[0] == id: 
			return node[1]['labels']['label']

def get_id(name, G):
	for node in list(G.nodes(data = True)):
		if node[1]['labels']['label'] == name: 
			return node[0]

#plot della distribuzione dei gradi 
def degree_plt(degree, title, show, fileName, save):
	values = []
	magg = -1
	id_max = 0;

	for v in degree:
		values.append(v[1])
		if v[1] > magg: 
			magg = v[1]
			id_max = v[0]

	y = []
	x = []
	ypl = []
	val,binEdges=np.histogram(values,bins=150)
	bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
	for i in range(0, len(val)):
		if val[i] != 0:
			y.append(val[i])
			x.append(bincenters[i])
			ypl.append(300 * bincenters[i]**-2)
	
	plt.figure(figsize=(8,6), dpi=250)
	plt.yscale("log")
	plt.plot(x, y, '.-', color = 'b')
	plt.plot(x, ypl, '.-', color = 'r')
	plt.title(title, fontsize = 22)
	plt.xlabel('Edges', fontsize = 15)
	plt.ylabel('Probability', fontsize = 15)
	plt.grid(False)
	plt.tight_layout()
	f_d = plt.gcf()
	if show:
		plt.show()
	if save:
		f_d.savefig(fileName)

	for node in list(M.nodes(data = True)):
		if node[0] == id_max: 
			return (node[1]['labels']['label'], magg)

	return -1
	
#calcolo di E
def calculateE(M):
	n = M.number_of_nodes()
	d = dict(nx.all_pairs_dijkstra_path_length(M))
	sum = 0
	for node1 in d:
		for node2 in d[node1]:
			if d[node1][node2] != 0:
				sum += 1/d[node1][node2]
	E = (1/(n*(n-1)))*sum
	return(E)

fileName = 'edges.csv'

M = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = [('line', str), ('trip_count', int)])

dicto = {}
dict_pos = {}
with open('nodes.csv') as csv_file:
	cvs_reader = csv.reader(csv_file, delimiter = ',')
	next(cvs_reader)

	for row in cvs_reader:
		dicto[row[0]] = {'label' : row[1]}
		dict_pos[row[0]] = (float(row[3]), float(row[2]))		

nx.set_node_attributes(M, dicto, 'labels')

#total degree 
degree = M.degree(weight = 'trip_count')
sorted_degree = sorted(degree, key=lambda tup: tup[1], reverse = True)

#betweenness
betweenness_centrality = nx.betweenness_centrality(M)
id_max = max(betweenness_centrality.items(), key=operator.itemgetter(1))[0]
sorted_bet = sorted(betweenness_centrality.items(), key=operator.itemgetter(1), reverse = True)


#tolgo il 50% dei nodi in base a betweenness e calcolo E e S
list_bet = sorted_bet[:int(M.number_of_nodes()/2)]

M2 = M.copy()
Evalues_bet = []
e_prec = calculateE(M2)
print("E totale: " + str(e_prec))
Evalues_bet.append(e_prec)
Svalues_bet = []
n_components_bet = []
s_prec = max(nx.weakly_connected_component_subgraphs(M2), key=len).number_of_nodes()/M2.number_of_nodes()
Svalues_bet.append(s_prec)
n_components_bet.append(1)
diff_e_bet = []
diff_s_bet = []
M_bet = []
#print(list_bet)
for i in range(0, len(list_bet)):
	M2.remove_node(list_bet[i][0])
	n_comp = len(list(nx.weakly_connected_component_subgraphs(M2)))
	if n_comp == 2 and M_bet == []:
		M_bet = M2.copy()
		print(i)
		print("stazione: " + get_name(list_bet[i][0], M))
	n_components_bet.append(n_comp)
	E = calculateE(M2)
	Evalues_bet.append(E)
	diff_e_bet.append((list_bet[i][0], e_prec - E))
	e_prec = E
	S = max(nx.weakly_connected_component_subgraphs(M2), key=len).number_of_nodes()/M2.number_of_nodes()
	Svalues_bet.append(S)
	diff_s_bet.append((list_bet[i][0], s_prec - S))
	s_prec = S
	

#tolgo il 50% dei nodi in base a degree e calcolo E e S
list_deg = sorted_degree[:int(M.number_of_nodes()/2)]

M3 = M.copy()
Evalues_deg = []
e_prec = calculateE(M3)
Evalues_deg.append(e_prec)
Svalues_deg = []
n_components_deg = []
s_prec = max(nx.weakly_connected_component_subgraphs(M3), key=len).number_of_nodes()/M3.number_of_nodes()
Svalues_deg.append(s_prec)
n_components_deg.append(1)
diff_e_deg = []
diff_s_deg = []
M_deg = []
for i in range(0, len(list_deg)):
	M3.remove_node(list_deg[i][0])
	n_comp = len(list(nx.weakly_connected_component_subgraphs(M3)))
	if n_comp == 2 and M_deg == []:
		M_deg = M3.copy()
		print(i)
		print("stazione: " + get_name(list_deg[i][0], M))
	n_components_deg.append(n_comp)
	E = calculateE(M3)
	Evalues_deg.append(E)
	diff_e_deg.append((list_deg[i][0], e_prec - E))
	e_prec = E
	S = max(nx.weakly_connected_component_subgraphs(M3), key=len).number_of_nodes()/M3.number_of_nodes()
	Svalues_deg.append(S)
	diff_s_deg.append((list_deg[i][0], s_prec - S))
	s_prec = S


max_s_bet = diff_s_bet[0]
max_s_deg = diff_s_deg[0]
max_e_bet = diff_e_bet[0]
max_e_deg = diff_e_deg[0]
for i in range(0, len(diff_e_bet)):
	if diff_e_bet[i][1] >= max_e_bet[1]:
		max_e_bet = diff_e_bet[i]
		x_e_bet = i
	if diff_s_bet[i][1] >= max_s_bet[1]:
		max_s_bet = diff_s_bet[i]
		x_s_bet = i
	if diff_e_deg[i][1] >= max_e_deg[1]:
		max_e_deg = diff_e_deg[i]
		x_e_deg = i
	if diff_s_deg[i][1] >= max_s_deg[1]:
		max_s_deg = diff_s_deg[i]
		x_s_deg = i	

print("\nE dopo eliminazione di 5 nodi")
print(Evalues_bet[5])
print(Evalues_deg[5])

print("\nS dopo eliminazione di 5 nodi")
print(Svalues_bet[5])
print(Svalues_deg[5])

print('\nMax s betweenness')
print(get_name(max_s_bet[0], M))
print(max_s_bet)
print(x_s_bet)
print('Max e betweenness')
print(get_name(max_e_bet[0], M))
print(max_e_bet)
print(x_e_bet)
print('Max s degree')
print(get_name(max_s_deg[0], M))
print(max_s_deg)
print(x_s_deg)
print('Max e degree')
print(get_name(max_e_deg[0], M))
print(max_e_deg)
print(x_e_deg)


nVal = [i for i in range(0, len(list_deg)+1)]

Evalues_bet_norm = [e/max(Evalues_bet) for e in Evalues_bet]
Evalues_deg_norm = [e/max(Evalues_deg) for e in Evalues_deg]

##plot E
plt.figure(figsize=(6,7), dpi=250)
plt.subplot(2, 1, 1)
plt.title('Decrescita di E', fontsize = 22)
plt.plot(nVal, Evalues_bet_norm, '.-', label = "Betweenness", color = 'b')
plt.plot(nVal, Evalues_deg_norm, '.-', label = "Degree", color = 'r')
plt.plot(x_e_bet, Evalues_bet_norm[x_e_bet], 'D', color = 'b')
plt.plot(x_e_deg, Evalues_deg_norm[x_e_deg], 'D', color = 'r')
plt.xlabel('Numero di stazioni rimosse', fontsize = 15)
plt.ylabel('E', fontsize = 15)
plt.legend()
plt.grid(False)
plt.tight_layout()



##plot S
plt.subplot(2, 1, 2)
plt.title('Decrescita di S', fontsize = 22)
plt.plot(nVal, Svalues_bet, '.-', label = "Betweenness", color = 'b')
plt.plot(nVal, Svalues_deg, '.-', label = "Degree", color = 'r')
plt.plot(x_s_bet, Svalues_bet[x_s_bet], 'D', color = 'b')
plt.plot(x_s_deg, Svalues_deg[x_s_deg], 'D', color = 'r')
plt.xlabel('Numero di stazioni rimosse', fontsize = 15)
plt.ylabel('S', fontsize = 15)
plt.legend()
plt.grid(False)
plt.tight_layout()
f_es = plt.gcf()
f_es.savefig("plot_E_S.pdf")

##plot aumento delle componenti connesse
plt.figure(figsize=(8,6), dpi=250)
f_cc = plt.gcf()
plt.title('Componenti connesse durante l\'attacco', fontsize = 20)
plt.plot(nVal, n_components_bet, '.-', label = "Betweenness", color = 'b')
plt.plot(nVal, n_components_deg, '.-', label = "Degree", color = 'r')
plt.xlabel('Numero di stazioni rimosse', fontsize = 15)
plt.ylabel('Numero di componenti connesse', fontsize = 15, rotation='vertical')
plt.legend()
plt.grid(False)
plt.tight_layout()
f_cc.savefig("plot_cc.pdf")


##grafo originale
plt.figure(figsize=(4, 3), dpi=300)
f_original = plt.gcf()
plt.title('Rete prima dell\'attacco')
nx.draw_networkx(M, dict_pos, with_labels=False, node_size=3, arrowsize=0.5, node_color = 'b')
f_original.savefig("grafo_originale.pdf")

##grafo appena si creano componenti disconnesse (degree)
plt.figure(figsize=(4, 3), dpi=300)
f_degree = plt.gcf()
plt.title('Rete dopo l\'attacco basato su Degree')
components = list(nx.weakly_connected_component_subgraphs(M_deg))
print('n componenti: ' + str(len(components)))
print(components[1].nodes(data = True))
colors = ['b','r','g','y']
for i in range(0, len(components)):
	nx.draw_networkx(components[i], dict_pos, with_labels=False, node_size=3, arrowsize=0.5, node_color = colors[i])
	
f_degree.savefig("grafo_attacco_degree.pdf")

##grafo appena si creano componenti disconnesse (betweenness)
plt.figure(figsize=(4, 3), dpi=300)
f_betweenness = plt.gcf()
plt.title('Rete dopo l\'attacco basato su Betweenness')
components = list(nx.weakly_connected_component_subgraphs(M_bet))
print('n componenti: ' + str(len(components)))

for i in range(0, len(components)):
	nx.draw_networkx(components[i], dict_pos, with_labels=False, node_size=3, arrowsize=0.5, node_color = colors[i])

f_betweenness.savefig("grafo_attacco_betweenness.pdf")


plt.show()
