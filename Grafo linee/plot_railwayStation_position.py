####Plot della posizione nella rete di una o più stazioni####
####usage: da riga di comando passare gli "stop_id" (contenuti nel file "nodes.csv") delle stazioni che si vogliono visualizzare####


import networkx as nx
import csv
import matplotlib.pyplot as plt
import operator
import sys


def get_name(id, G):
	name = None
	for node in list(G.nodes(data = True)):
		if node[0] == id: 
			name = node[1]['labels']['label']
	return name
def get_id(name, G):
	for node in list(G.nodes(data = True)):
		if node[1]['labels']['label'] == name: 
			return node[0]
			
if __name__ == '__main__':

	fileName = 'edges.csv'
	M = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = [('line', str), ('trip_count', int)])
	dicto = {}
	dict_pos = {}
	with open('nodes.csv') as csv_file:
		cvs_reader = csv.reader(csv_file, delimiter = ',')
		next(cvs_reader)

		for row in cvs_reader:
			dicto[row[0]] = {'label' : row[1]}
			dict_pos[row[0]] = (float(row[3]), float(row[2]))		
	nx.set_node_attributes(M, dicto, 'labels')
	
	plt.figure(figsize=(4, 3), dpi=300)
	f_e_deg = plt.gcf()
	for i in range(1, len(sys.argv)):
		name = get_name(str(sys.argv[i]), M)
		if name == None:
			print("ID non valido")
			sys.exit()
		else:
			plt.title('Posizione di '+ name)
			nx.draw_networkx(M, dict_pos, with_labels=False, node_size=2, arrowsize=0.5, node_color = 'b')
			nx.draw_networkx(M, dict_pos, node_size=8, arrowsize=0.5, nodelist = [str(sys.argv[i])], labels = {str(sys.argv[i]) : name}, node_color = 'r', edgelist = [], font_size = 2, font_weight='bold')
	plt.show()