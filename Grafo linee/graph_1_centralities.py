####analisi di centralità del grafo delle linee####
####1. In-degree, out-degree e Total Degree####
####2. Closeness####
####3. Betweenness####


import networkx as nx
import csv
import matplotlib.pyplot as plt
import operator
import numpy as np

def get_name(id, G):
	for node in list(G.nodes(data = True)):
		if node[0] == id: 
			return node[1]['labels']['label']

def get_id(name, G):
	for node in list(G.nodes(data = True)):
		if node[1]['labels']['label'] == name: 
			return node[0]

def degree_plt_max(degree, title, show, fileName, save):
	values = []
	magg = -1
	id_max = 0;

	for v in degree:
		values.append(v[1])
		if v[1] > magg: 
			magg = v[1]
			id_max = v[0]

	y = []
	x = []
	val,binEdges=np.histogram(values,bins=100)
	bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
	for i in range(0, len(val)):
		if val[i] != 0:
			y.append(val[i])
			x.append(bincenters[i])
	plt.figure(figsize=(6,5), dpi=200)
	#plt.yscale("log")
	width = 5*len(binEdges)/100
	plt.bar(x, y, width = 4, log = True)
	plt.title(title, fontsize = 22)
	plt.xlabel('Degree', fontsize = 15)
	plt.ylabel('Frequenza', fontsize = 15)
	plt.grid(False)
	plt.tight_layout()
	f_d = plt.gcf()
	if show:
		plt.show()
	if save:
		plt.savefig(fileName)

	for node in list(M.nodes(data = True)):
		if node[0] == id_max: 
			return (node[1]['labels']['label'], magg)

	return -1

fileName = 'edges.csv'

M = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = [('line', str), ('trip_count', int)])

dicto = {}
dict_pos = {}
with open('nodes.csv') as csv_file:
	cvs_reader = csv.reader(csv_file, delimiter = ',')
	next(cvs_reader)

	for row in cvs_reader:
		dicto[row[0]] = {'label' : row[1]}
		dict_pos[row[0]] = (float(row[3]), float(row[2]))

nx.set_node_attributes(M, dicto, 'labels')

#degree 
degree = M.degree(weight = 'trip_count')
print(degree_plt_max(degree, "Distribuzione Total Degree", False, "total_degree_original.pdf", True))

sorted_degree = sorted(degree, key=lambda tup: tup[1], reverse = True)

print("\ntotal degree")
for el in sorted_degree[: 5]:
	print(get_name(el[0], M)+ ' ' + str(el[1]))
print([el[0] for el in sorted_degree[:10]])

plt.figure(figsize=(4, 3), dpi=300)
nx.draw_networkx(M, dict_pos, with_labels = False, node_size = [el[1]/50 for el in degree], nodelist = [el[0] for el in degree], arrowsize = 0.5, node_color = 'b')#, labels = nodes, font_size = 2, font_weight='bold')
plt.savefig("grafo_degree.png")

#indegree
degree = M.in_degree(weight = 'trip_count')
print(degree_plt_max(degree, "Distribuzione In Degree", False, "indegree_original.pdf", True))

sorted_degree = sorted(degree, key=lambda tup: tup[1], reverse = True)
print("\nindegree")
for el in sorted_degree[: 5]:
	print(get_name(el[0], M)+ ' ' + str(el[1]))

#outdegree
degree = M.out_degree(weight = 'trip_count')
print(degree_plt_max(degree, "Distribuione Out Degree", False, "outdegree_original.pdf", True))


sorted_degree = sorted(degree, key=lambda tup: tup[1], reverse = True)
print("\noutdegree")
for el in sorted_degree[: 5]:
	print(get_name(el[0], M) + ' ' + str(el[1]))
plt.show()

#closeness 
closeness_centrality = nx.closeness_centrality(M)

y = []
x = []
val,binEdges=np.histogram(list(closeness_centrality.values()),bins=100)
bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
for i in range(0, len(val)):
	if val[i] != 0:
		y.append(val[i])
		x.append(bincenters[i])
plt.figure(figsize=(6,5), dpi=200)
width = 1*len(list(closeness_centrality.values()))/100
plt.bar(x, y, width = 0.0007, log = True)
plt.title("Distribuzione Closeness", fontsize = 22)
plt.xlabel("Closeness", fontsize = 15)
plt.ylabel("Frequenza", fontsize = 15)
plt.grid(False)
plt.tight_layout()
f_d = plt.gcf()
plt.savefig("closeness_original.pdf")
plt.figure(figsize=(4, 3), dpi=300)
nx.draw_networkx(M, dict_pos, with_labels = False, node_size = [v*20 for v in closeness_centrality.values()], nodelist = [k for k in closeness_centrality.keys()], arrowsize = 0.5, node_color = 'b')#, labels = nodes, font_size = 2, font_weight='bold')
plt.savefig("grafo_closeness.png")
id_max = max(closeness_centrality.items(), key=operator.itemgetter(1))[0]

print(get_name(id_max, M))

sorted_closeness = sorted(closeness_centrality.items(), key=operator.itemgetter(1), reverse = True)

print("\nCloseness")
for el in sorted_closeness[: 5]:
	print(get_name(el[0], M)+ ' ' + str(el[1]))

#betweenness_centrality
betweenness_centrality = nx.betweenness_centrality(M)

y = []
x = []
val,binEdges=np.histogram(list(betweenness_centrality.values()),bins=100)
bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
for i in range(0, len(val)):
	if val[i] != 0:
		y.append(val[i])
		x.append(bincenters[i])
plt.figure(figsize=(6,5), dpi=200)
width = 1*len(list(betweenness_centrality.values()))/100
plt.bar(x, y, width = 0.003, log = True)
plt.title("Distribuzione Betweeness", fontsize = 22)
plt.xlabel("Betweeenness", fontsize = 15)
plt.ylabel("Frequenza", fontsize = 15)
plt.grid(False)
plt.tight_layout()
f_d = plt.gcf()
plt.savefig("betweenness_original.pdf")

plt.figure(figsize=(4, 3), dpi=300)
nx.draw_networkx(M, dict_pos, with_labels = False, node_size = [v*20 for v in betweenness_centrality.values()], nodelist = [k for k in betweenness_centrality.keys()], arrowsize = 0.5, node_color = 'b')#, labels = nodes, font_size = 2, font_weight='bold')
plt.savefig("grafo_betweenness.png")

id_max = max(betweenness_centrality.items(), key=operator.itemgetter(1))[0]

sorted_bet = sorted(betweenness_centrality.items(), key=operator.itemgetter(1), reverse = True)

print("\nBetweeness")
for el in sorted_bet[: 5]:
	print(get_name(el[0], M)+ ' ' + str(el[1]))
print([el[0] for el in sorted_bet[:10]])

nodes = {}
for n in M.nodes(data = True):
	if n[0] in [i[0] for i in sorted_bet[:4]]:
		nodes[n[0]] =  n[1]['labels']['label']


plt.figure(figsize=(4, 3), dpi=300)
plt.title("Top 4 Betweeness")
nx.draw_networkx(M, dict_pos, node_size = 3, with_labels = False, arrowsize=0.5, node_color = 'b')
nx.draw_networkx(M, dict_pos, node_size = 10, arrowsize = 0.5, node_color = 'r', nodelist = [i[0] for i in sorted_bet[:4]], with_labels = False)#, labels = nodes, font_size = 2, font_weight='bold')

plt.figure(figsize=(4, 3), dpi=300)
plt.title("Top 4 Degree")
nx.draw_networkx(M, dict_pos, node_size = 3, with_labels = False, arrowsize=0.5, node_color = 'b')
nx.draw_networkx(M, dict_pos, node_size = 10, arrowsize = 0.5, node_color = 'r', nodelist = [i[0] for i in sorted_degree[:4]], with_labels = False)#, labels = nodes, font_size = 2, font_weight='bold')

plt.figure(figsize=(4, 3), dpi=300)
plt.title("Top 4 Closeness")
nx.draw_networkx(M, dict_pos, node_size = 3, with_labels = False, arrowsize=0.5, node_color = 'b')
nx.draw_networkx(M, dict_pos, node_size = 10, arrowsize = 0.5, node_color = 'r', nodelist = [i[0] for i in sorted_closeness[:4]], with_labels = False)#, labels = nodes, font_size = 2, font_weight='bold')

plt.show()