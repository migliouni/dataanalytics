####clacolo degli shortest path per ogni stazione del grafo delle linee e plot della posizione delle stazioni con peggiori SP####
####1. SP rete originale####
####2. SP rete rimosso Treviglio####
####3. SP rete rimosso Milano Rogoredo####
####4. SP rete rimosso Milano Lambrate####

####plot della rete dopo la rimozione dei nodi che causano massima decrescita in E e S####
####1. rimozione Milano Rogoredo####
####2. rimozione Gallarate####
####3. rimozione Treviglio####


import networkx as nx
import csv
import matplotlib.pyplot as plt
import operator
import numpy as np
import random


def get_name(id, G):
	for node in list(G.nodes(data = True)):
		if node[0] == id: 
			return node[1]['labels']['label']

def get_id(name, G):
	for node in list(G.nodes(data = True)):
		if node[1]['labels']['label'] == name: 
			return node[0]

def plot_shortestPath(G, title):	
	max = ('0',0)
	pathlengths = []
	for v in G.nodes():
		spl = dict(nx.single_source_dijkstra_path_length(G, v))
		for k,v in spl.items():
			if spl[k] != 0:
				pathlengths.append(spl[k])
			if v > max[1]:
				max = (k,v)
	print(max)
	print(get_name(max[0], G))
	print("lenght media: " + str(nx.average_shortest_path_length(G, method='dijkstra')))
	dist = {}
	for p in pathlengths:
		if p in dist:
			dist[p] += 1
		else:
			dist[p] = 1
	bins = []
	values = []
	verts = dist.keys()
	for d in sorted(verts):
		values.append(dist[d])
		bins.append(d)
	print(bins[-1])
	plt.yscale("log")
	plt.plot(bins, values, '.-', label = title)
	plt.title("Distribuzione degli Shortest Path", fontsize = 22)
	plt.xlabel('Hops', fontsize = 15)
	plt.ylabel('Frequenza', fontsize = 15)
	plt.grid(False)
	plt.tight_layout()
	

fileName = 'edges.csv'

M = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = [('line', str), ('trip_count', int)])

dicto = {}
dict_pos = {}
with open('nodes.csv') as csv_file:
	cvs_reader = csv.reader(csv_file, delimiter = ',')
	next(cvs_reader)

	for row in cvs_reader:
		dicto[row[0]] = {'label' : row[1]}
		dict_pos[row[0]] = (float(row[3]), float(row[2]))		

nx.set_node_attributes(M, dicto, 'labels')

M2 = M.copy()
M3 = M.copy()
M_s_bet = M.copy()
M_e_bet = M.copy()
M_e_deg = M.copy()
M_s_deg = M.copy()

M_s_bet.remove_node('1393')#gallarate
M_e_bet.remove_node('1712')#lambrate
M_s_deg.remove_node('2919')#treviglio
M_e_deg.remove_node('1720')#rogoredo

f = plt.figure(figsize=(8,5), dpi=200)
print('completo')
plot_shortestPath(M, 'rete completa')
print('no treviglio')
plot_shortestPath(M_s_deg, 'senza Treviglio')
print('no rogoredo')
plot_shortestPath(M_e_deg, 'senza Milano Rogoredo')
print('no lambrate')
plot_shortestPath(M_e_bet, 'senza Milano Lambrate')
M_e_deg.remove_node('1712')
plot_shortestPath(M_e_deg, 'senza Lambrate e Rogoredo')
plt.legend(prop={'size': 8})
#f.savefig("plot_shortestPaths.pdf")

nodes = []
for n in M.nodes(data = True):
	if n[0] == '5051':
		nodes.append({'5051': n[1]['labels']['label']})
	else:
		if n[0] == '2481':
			nodes.append({'2481': n[1]['labels']['label']})
		else:
			if n[0] == '1593':
				nodes.append({'1593': n[1]['labels']['label']})


plt.figure(figsize=(4, 3), dpi=300)
f_e_deg = plt.gcf()
plt.title('Posizione di EDOLO')
nx.draw_networkx(M, dict_pos, with_labels=False, node_size=3, arrowsize=1, node_color = 'b')
nx.draw_networkx(M, dict_pos, node_size=10, arrowsize=0.5, nodelist = ['5051'], labels = nodes[2], node_color = 'r', edgelist = [], font_size = 2, font_weight='bold')
#f_e_deg.savefig("grafo_edolo.pdf")

plt.figure(figsize=(4, 3), dpi=300)
f_e_deg = plt.gcf()
plt.title('Posizione di LUINO')
nx.draw_networkx(M_s_deg, dict_pos, with_labels=False, node_size=3, arrowsize=1, node_color = 'b')
nx.draw_networkx(M_s_deg, dict_pos, node_size=10, arrowsize=0.5, nodelist = ['1593'], labels = nodes[0], node_color = 'r', edgelist = [], font_size = 2, font_weight='bold')
#f_e_deg.savefig("grafo_luino.pdf")

plt.figure(figsize=(4, 3), dpi=300)
f_e_deg = plt.gcf()
plt.title('Posizione di S.CRISTINA e BISSONE')
nx.draw_networkx(M_e_deg, dict_pos, with_labels=False, node_size=3, arrowsize=1, node_color = 'b')
nx.draw_networkx(M_e_deg, dict_pos, node_size=10, arrowsize=0.5, nodelist = ['2481'], labels = nodes[1], node_color = 'r', edgelist = [], font_size = 2, font_weight='bold')
#f_e_deg.savefig("grafo_bissone.pdf")


##grafo rimosso nodo con salto maggiore in E (betweenness)
plt.figure(figsize=(4, 3), dpi=300)
f_e_deg = plt.gcf()
plt.title('Rete dopo rimozione di MILANO ROGOREDO')
components = list(nx.weakly_connected_component_subgraphs(M_e_deg))
print(len(components))
colors = ['b','r','g']
for i in range(0, len(components)):
	nx.draw_networkx(components[i], dict_pos, with_labels=False, node_size=3, arrowsize=0.5, node_color = colors[i])
	
#f_e_deg.savefig("grafo_e_betweenness.pdf")

##grafo rimosso nodo con salto maggiore in S e in E (degree)
plt.figure(figsize=(4, 3), dpi=300)
f_s_deg = plt.gcf()
plt.title('Rete dopo rimozione di TREVIGLIO')
components = list(nx.weakly_connected_component_subgraphs(M_s_deg))
print(len(components))
for i in range(0, len(components)):
	nx.draw_networkx(components[i], dict_pos, with_labels=False, node_size=3, arrowsize=0.5, node_color = colors[i])
	
#f_s_deg.savefig("grafo_es_degree.pdf")

##grafo rimosso nodo con salto maggiore in S (betweenness)
plt.figure(figsize=(4, 3), dpi=300)
f_s_bet = plt.gcf()
plt.title('Rete dopo rimozione di GALLARATE')
components = list(nx.weakly_connected_component_subgraphs(M_s_bet))
print(len(components))
for i in range(0, len(components)):
	nx.draw_networkx(components[i], dict_pos, with_labels=False, node_size=3, arrowsize=0.5, node_color = colors[i])
	
#f_s_bet.savefig("grafo_s_betweenness.pdf")


plt.show()
