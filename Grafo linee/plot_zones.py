####plot delle stazioni in base alle zone a cui appartengono####


import networkx as nx
import csv
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import operator
import numpy as np

def filter_line(M, line_name):
	line = []
	for e in M.edges(data = True):
		if e[2]['line'] == line_name:
			line.append((e[0], e[1]))
	return line	
	
fileName = 'edges.csv'

M = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = [('line', str), ('trip_count', int)])

dicto = {}
dict_pos = {}
with open('nodes.csv') as csv_file:
	cvs_reader = csv.reader(csv_file, delimiter = ',')
	next(cvs_reader)

	for row in cvs_reader:
		dicto[row[0]] = {'label' : row[1]}
		dict_pos[row[0]] = (float(row[3]), float(row[2]))		

file ="zone.txt"
with open(file, 'r') as f:
	s = f.read()
	timesBase = eval(s)


nx.set_node_attributes(M, dicto, 'labels')
dict_colors = {'R1': '#1FCA1F', 'R11': '#352812', 'R12': '#3647DD', 'R13': '#6E4A65', 'R14': '#D62011', 'R16': '#3EE176', 'R17': '#0047B7', 'R18': '#BF5EB1', 'R2': '#F5130C', 'R21': '#D653B4', 'R22': '#914ED3', 'R23': '#A71567', 'R25': '#C186D0', 'R27': '#50AAE2', 'R28': '#C1FBE5', 'R3': '#044872', 'R31': '#30F120', 'R32': '#60E42E', 'R34': '#824AA2', 'R35': '#10B0A2', 'R36': '#26DB0F', 'R37': '#A0001E', 'R38': '#C368EA', 'R39': '#D49545', 'R4': '#DDE94C', 'R40': '#DA47C1', 'R41': '#CEDA05', 'R5': '#A7B525', 'R6': '#B059BA', 'R7': '#9FB970', 'R8': '#B56FF5', 'RE_1': '#8D9BBC', 'RE_10': '#A0AC2B', 'RE_11': '#EE5C17', 'RE_13': '#FF64F5', 'RE_2': '#DDF3DD', 'RE_3': '#797AA8', 'RE_4': '#D3504E', 'RE_5': '#BF587D', 'RE_6': '#627CF4', 'RE_7': '#3CB7B5', 'RE_8': '#BAF798', 'S1': '#E4A098', 'S10': '#825F5D', 'S11': '#6D52B8', 'S12': '#39A5B8', 'S13': '#FAB705', 'S2': '#992B90', 'S3': '#CBBC48', 'S4': '#C5E07D', 'S40': '#4D4758', 'S5': '#B108A5', 'S50': '#07DAEB', 'S6': '#9D85B4', 'S7': '#004C84', 'S8': '#6F3BFB', 'S9': '#9602B3', 'XP1': '#D223EC', 'XP2': '#972CAB'}
plt.figure(figsize=(4, 3), dpi=300)
plt.title("Divisione della rete in zone")
for l in dict_colors.keys():
	l_edges = filter_line(M, l)
	nx.draw_networkx(M, dict_pos, with_labels=False, node_size=2, arrowsize=0.5, edgelist = l_edges, edge_color = dict_colors[l])

for k in timesBase.keys():
	if timesBase[k][1] == 1:
		nx.draw_networkx(M, dict_pos, with_labels=False, node_size=3, edgelist = [], nodelist = [k], node_color = '#EE5A24')
	else:
		if timesBase[k][1] == 2:
			nx.draw_networkx(M, dict_pos, node_size=3, with_labels=False, edgelist = [], nodelist = [k], node_color = '#009432')
		else:
			if timesBase[k][1] == 3:
				nx.draw_networkx(M, dict_pos, with_labels=False, node_size=3, edgelist = [], nodelist = [k], node_color = '#0652DD')
			else:
				if timesBase[k][1] == 4:
					nx.draw_networkx(M, dict_pos, with_labels=False, node_size=3, edgelist = [], nodelist = [k], node_color = '#9980FA')
				else:
					if timesBase[k][1] == 5:
						nx.draw_networkx(M, dict_pos, with_labels=False, node_size=3, edgelist = [], nodelist = [k], node_color = '#833471')
	
z1_patch = mpatches.Patch(color='#EE5A24', label='Zona 1')
z2_patch = mpatches.Patch(color='#009432', label='Zona 2')
z3_patch = mpatches.Patch(color='#0652DD', label='Zona 3')
z4_patch = mpatches.Patch(color='#9980FA', label='Zona 4')
z5_patch = mpatches.Patch(color='#833471', label='Zona 5')
plt.legend(handles=[z1_patch, z2_patch, z3_patch, z4_patch, z5_patch], prop={'size': 4})

#plt.savefig("grafo_distanze.pdf")
plt.show()
