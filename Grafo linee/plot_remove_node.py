####plot della rete rimuovendo una o più stazioni, il colore di esse è dato dalla componente connessa a cui appartiene####
####usage: da riga di comando passare gli "stop_id" (contenuti nel file "nodes.csv") delle stazioni da rimuovere####


import networkx as nx
import csv
import matplotlib.pyplot as plt
import operator
import sys
import random

def get_name(id, G):
	name = None
	for node in list(G.nodes(data = True)):
		if node[0] == id: 
			name = node[1]['labels']['label']
	return name
def get_id(name, G):
	for node in list(G.nodes(data = True)):
		if node[1]['labels']['label'] == name: 
			return node[0]
			
if __name__ == '__main__':

	fileName = 'edges.csv'
	M = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = [('line', str), ('trip_count', int)])
	dicto = {}
	dict_pos = {}
	with open('nodes.csv') as csv_file:
		cvs_reader = csv.reader(csv_file, delimiter = ',')
		next(cvs_reader)

		for row in cvs_reader:
			dicto[row[0]] = {'label' : row[1]}
			dict_pos[row[0]] = (float(row[3]), float(row[2]))		
	nx.set_node_attributes(M, dicto, 'labels')
	
	plt.figure(figsize=(4, 3), dpi=300)
	f_e_deg = plt.gcf()
	for i in range(1, len(sys.argv)):
		name = get_name(str(sys.argv[i]), M)
		if name == None:
			print("ID non valido")
			sys.exit()
		M.remove_node(str(sys.argv[i]))
		components = list(nx.weakly_connected_component_subgraphs(M))
		print(len(components))
		print(components)
		for sg in components:
			r = lambda: random.randint(0,255)
			color = '#%02X%02X%02X' % (r(),r(),r())
			nx.draw_networkx(M, dict_pos, node_size=4, arrowsize=0.5, nodelist = sg, with_labels = False, node_color = color)
	plt.show()