import csv
from math import sin, cos, sqrt, atan2, radians

#calcola e ritorna le distanze dal nodo centrale, per poter stabilire in che zona si trova ogni stazione
#station stazione centrale da cui calcore le distanze, radius sono una lista di raggi per la divisione in zone
def distanceFromStation(station, radius):
	readFile = "nodes.csv"
	nodes = {}
	names = {}
	with open(readFile) as csv_file:
		cvs_reader = csv.reader(csv_file, delimiter = ',')
		next(cvs_reader)
		for row in cvs_reader:
			nodes[row[0]] = (float(row[2]),float(row[3]))
			names[row[1]] = row[0]
	
	c20 = 0
	c30 = 0
	c50 = 0
	c70 = 0
	coltre = 0
	stationid = names[station]
	latBase = radians(nodes[stationid][0])
	lonBase = radians(nodes[stationid][1])
	R = 6373.0
	distances = {}
	
	for node in nodes:
		lat2 = radians(nodes[node][0])
		lon2 = radians(nodes[node][1])
		
		dlat = lat2 - latBase
		dlon = lon2 - lonBase
		a = sin(dlat / 2)**2 + cos(latBase) * cos(lat2) * sin(dlon / 2)**2
		c = 2 * atan2(sqrt(a), sqrt(1 - a))

		distances[node] =  round(R * c, 3)
		if distances[node] <= radius[0]:
			c20 = c20 + 1
			distances[node] = (distances[node], 1)
		else:
			if distances[node] <= radius[1]:
				c30 = c30 + 1
				distances[node] = (distances[node], 2)
			else:
				if distances[node] <= radius[2]:
					c50 = c50 + 1
					distances[node] = (distances[node], 3)
				else:
					if distances[node] <= radius[3]:
						c70 = c70 + 1
						distances[node] = (distances[node], 4)
					else:
						coltre = coltre +1
						distances[node] = (distances[node], 5)
	
	'''fileWrite = "zone.txt"
	out_file = open(fileWrite,"w")
	out_file.write(str(distances))
	out_file.close()
	'''
	return distances
	


	
		