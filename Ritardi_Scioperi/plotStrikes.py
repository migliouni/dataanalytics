import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import operator

#Metodi che permettono di visualizzare su plot i dati prima e dopo ogni evento


#metodo che visualizza al variare del tasso di sciopero, l'andamento in ogni zona
def showStrikesData():
	plt.figure(figsize = (8, 6))
	probability = [0,10, 20, 30, 40, 50, 60]
	allResults = {}
	file ="ANALISIStrike.txt"
	with open(file, 'r') as f:
		s = f.read()
		allResults = eval(s)[0]
		allInf = eval(s)[1]
	
	plt.plot(probability,allResults[1], 's-', color='#EE5A24' )
	plt.plot(probability,allResults[2], 's-', color='#009432')
	plt.plot(probability,allResults[3], 's-', color='#0652DD')
	plt.plot(probability,allResults[4], 's-', color='#9980FA')
	plt.plot(probability,allResults[5], 's-', color='#833471')
	plt.xticks(probability, probability, rotation= 'vertical')
	plt.ylabel('Media dei tempi in secondi')
	plt.xlabel('Tasso di adesione')
	plt.title('Simulazione sciopero')
	plt.subplots_adjust(bottom=0.33)

	zona1 = mlines.Line2D([], [], color='#EE5A24', marker='s', markersize=5, label='Zona 1')

	zona2 = mlines.Line2D([], [], color='#009432', marker='s',markersize=5, label='Zona 2')
							 
	zona3 = mlines.Line2D([], [], color='#0652DD', marker='s', markersize=5, label='Zona 3')

	zona4 = mlines.Line2D([], [], color='#9980FA', marker='s',markersize=5, label='Zona 4')
							  
	zona5 = mlines.Line2D([], [], color='#833471', marker='s',markersize=5, label='Zona 5')

	plt.legend(handles=[zona1, zona2, zona3, zona4, zona5])
	plt.savefig('strikes.pdf')
	plt.show()
	
	
	#stazioni irragiugibile al variare del tasso di adesione
	plt.figure(figsize = (8, 6))
	plt.plot(probability,allInf[1], 's-', color='#EE5A24')
	plt.plot(probability,allInf[2], 's-', color='#009432')
	plt.plot(probability,allInf[3], 's-', color='#0652DD')
	plt.plot(probability,allInf[4], 's-', color='#9980FA')
	plt.plot(probability,allInf[5], 's-', color='#833471')
	plt.xticks(probability, probability, rotation= 'vertical')
	plt.ylabel('Numero di stazioni irraggiungibili')
	plt.xlabel('Tasso di adesione')
	plt.title('Simulazione sciopero')
	plt.subplots_adjust(bottom=0.33)

	plt.legend(handles=[zona1, zona2, zona3, zona4, zona5])
	plt.savefig('strikesinf.pdf')
	plt.show()

#Metodo che visualizza al variare dell'ora del giorno le meide per ogi zona
def showTrainsAllHours():
	plt.figure(figsize = (8, 6))

	file = "ANALISIALLHOURS.txt"
	with open(file, 'r') as f:
		s = f.read()
		dizio = eval(s)[0]
		dizioInf = eval(s)[1]
	z1= []
	z2 =[]
	z3= []
	z4 =[]
	z5= []
	hours = []
	for hour in dizio.keys():
		z1.append(dizio[hour][0])
		z2.append(dizio[hour][1])
		z3.append(dizio[hour][2])
		z4.append(dizio[hour][3])
		z5.append(dizio[hour][4])
		hours.append(hour)
	
	plt.plot(hours,z1, 's-', color='#EE5A24')
	plt.plot(hours,z2, 's-', color='#009432')
	plt.plot(hours,z3, 's-', color='#0652DD')
	plt.plot(hours,z4, 's-', color='#9980FA')
	plt.plot(hours,z5, 's-', color='#833471')
	plt.xticks(hours, hours, rotation= 'vertical')
	plt.ylabel('Media dei tempi in secondi')
	plt.xlabel('Ore del giorno')
	plt.title("Analisi dei tempi medi al variare dell'orario")
	plt.subplots_adjust(bottom=0.33)

	zona1 = mlines.Line2D([], [], color='#EE5A24', marker='s', markersize=5, label='Zona 1')
	zona2 = mlines.Line2D([], [], color='#009432', marker='s', markersize=5, label='Zona 2')
	zona3 = mlines.Line2D([], [], color='#0652DD', marker='s', markersize=5, label='Zona 3')
	zona4 = mlines.Line2D([], [], color='#9980FA', marker='s', markersize=5, label='Zona 4')
	zona5 = mlines.Line2D([], [], color='#833471', marker='s', markersize=5, label='Zona 5')

	plt.legend(handles=[zona1, zona2, zona3, zona4, zona5])
	plt.savefig('allHours.pdf')
	plt.show()
	
	#gestione numero infiniti
	z1= []
	z2 =[]
	z3= []
	z4 =[]
	z5= []
	hours = []
	for hour in dizio.keys():
		z1.append(dizioInf[hour][0])
		z2.append(dizioInf[hour][1])
		z3.append(dizioInf[hour][2])
		z4.append(dizioInf[hour][3])
		z5.append(dizioInf[hour][4])
		hours.append(hour)
	
	plt.plot(hours,z1, 's-', color='#EE5A24')
	plt.plot(hours,z2, 's-', color='#009432')
	plt.plot(hours,z3, 's-', color='#0652DD')
	plt.plot(hours,z4, 's-', color='#9980FA')
	plt.plot(hours,z5, 's-', color='#833471')
	plt.xticks(hours, hours, rotation= 'vertical')
	plt.ylabel('Numero di stazioni irragiugibili')
	plt.xlabel('Ore del giorno')
	plt.title('Analisi stazioni irraggiungibili al variare dell\'orario')
	plt.subplots_adjust(bottom=0.33)
	
	plt.legend(handles=[zona1, zona2, zona3, zona4, zona5])
	plt.savefig('allHoursInf.pdf')
	plt.show()


#Metodo che permette di visualizzare i tempi medi in ogni zona prima e dopo l'eliminazione delle
#stazioni
def showDeleteStationsData():
	plt.figure(figsize = (8, 6))

	file = "ANALISIDeleteStations.txt"
	with open(file, 'r') as f:
		s = f.read()
		allData = eval(s)[0]
		stations = eval(s)[1]

	plt.plot(stations,allData[1], 's-', color='#EE5A24')
	plt.plot(stations,allData[2], 's-', color='#009432')
	plt.plot(stations,allData[3], 's-', color='#0652DD')
	plt.plot(stations,allData[4], 's-', color='#9980FA')
	plt.plot(stations,allData[5], 's-', color='#833471')
	plt.xticks(stations, stations, rotation= 'vertical')
	plt.ylabel('Tempo in secondi')
	plt.xlabel('STAZIONI ELIMINATE')
	plt.title('Simulazione malfunzionamento stazioni')
	plt.subplots_adjust(bottom=0.33)
	
	zona1 = mlines.Line2D([], [], color='#EE5A24', marker='s', markersize=5, label='Zona 1')
	zona2 = mlines.Line2D([], [], color='#009432', marker='s', markersize=5, label='Zona 2')
	zona3 = mlines.Line2D([], [], color='#0652DD', marker='s', markersize=5, label='Zona 3')
	zona4 = mlines.Line2D([], [], color='#9980FA', marker='s', markersize=5, label='Zona 4')
	zona5 = mlines.Line2D([], [], color='#833471', marker='s', markersize=5, label='Zona 5')
	
	plt.legend(handles=[zona1, zona2, zona3, zona4, zona5])
	plt.savefig('deletingStations.pdf')
	plt.show()
	

#Metodo che permette di visualizzare 
def showDelayCongestione(file):
	plt.figure(figsize = (8, 6))
	station = ["Normal"]
	with open(file, 'r') as f:
		s = f.read()
		allData = eval(s)[0]
		station.append(eval(s)[1])
	
	first = []
	post = []
	for key,value in allData.items():
		first.append(allData[key][0])
		post.append(allData[key][1])
	
	plt.bar(range(1,6),post)
	plt.bar(range(1,6),first)
	plt.ylabel('Medie dei tempi in secondi')
	plt.xlabel('Zone')
	plt.title('Introduzione ritardo dovuto alla congestione')
	

	Dopo = mlines.Line2D([], [], color='blue', marker='s',
							  markersize=5, label='Dopo')
	Prima = mlines.Line2D([], [], color='orange', marker='s',
							  markersize=5, label='Prima')
	plt.legend(handles=[Prima, Dopo])
	plt.savefig('test.pdf')
	plt.show()
	

#metodo 
def showDelayData():
	plt.figure(figsize = (8, 6))
	station = ["Normal"]
	
	file = "ANALISIDelayStation.txt"
	with open(file, 'r') as f:
		s = f.read()
		allData = eval(s)[0]
		station.append(eval(s)[1])
	
	first = []
	post = []
	for key,value in allData.items():
		first.append(allData[key][0])
		post.append(allData[key][1])
	
	plt.bar(range(1,6),post)
	plt.bar(range(1,6),first)
	plt.ylabel('Tempo medi in secondi')
	plt.xlabel('Zone')
	plt.title('Simulazione Malfunzionamento stazione')
	

	Dopo = mlines.Line2D([], [], color='blue', marker='s',
							  markersize=5, label='Dopo')
	Prima = mlines.Line2D([], [], color='orange', marker='s',
							  markersize=5, label='Prima')
	plt.legend(handles=[Prima, Dopo])
	#plt.show()
	plt.savefig('delayStation.pdf')

def showRemoveStationData():
	plt.figure(figsize = (8, 6))
	station = ["Normal"]
	
	file = "prova.txt"
	with open(file, 'r') as f:
		s = f.read()
		allData = eval(s)[0]
		station.append(eval(s)[1])
	
	first = []
	post = []
	for key,value in allData.items():
		first.append(allData[key][0])
		post.append(allData[key][1])
	
	plt.bar(range(1,6),post)
	plt.bar(range(1,6),first)
	plt.ylabel('Tempo medi in secondi')
	plt.xlabel('Zone')
	plt.title('rimozione stazione')
	

	Dopo = mlines.Line2D([], [], color='blue', marker='s',
							  markersize=5, label='Dopo')
	Prima = mlines.Line2D([], [], color='orange', marker='s',
							  markersize=5, label='Prima')
	plt.legend(handles=[Prima, Dopo])
	plt.show()
	#plt.savefig('.pdf')
	
'''
if __name__ == '__main__':
	#showTrainsAllHours()
	#showDeleteStationsData()
	#showStrikesData()
	#showDelayData()
	#showRemoveStationsInZone()
	showDelayCongestione()
'''