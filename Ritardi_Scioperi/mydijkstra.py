import operator
from collections import deque
import networkx as nx
import time


#string_time: variabile in formato HH:MM:SS
#converte string_time in secondi
#return time: ora in secondi
def stringToTime(string_time):
	string_split = string_time.split(":")
	time = 0
	if string_split[0] != 'XX':
		time = int(string_split[0]) * 3600
		time += int(string_split[1]) * 60
		time += int(string_split[2])
	else:
		time = -1
	return time 

def timeToString(second_time):
	return time.strftime('%H:%M:%S', time.gmtime(second_time))

#distances: distanze migliori dal nodo sorgente
#seen: lista di  nodi già visitati
#calcola il nodo con distanza minore tra quelli che non compaiono nella lista dei nodi già visti
#return: tupla (id del nodo, distanza dalla sorgente)
def min_not_seen(distances, seen):
	min = float('inf')
	min_node = 0
	for node in distances:
		if node not in seen and distances[node] < min:
			min = distances[node] 
			min_node = node

	return (min_node, min)

#G: grafo
#start_time: ora in secondi in cui arrivo nella stazione
#current_node: stazione in cui mi trovo
#starts: dizionario (key: nodi) (value: orario migliore da cui partire in quella stazione)
#lines: dizionario (key:nodi) (value: migliore linea entrante al nodo)
#all_lines: dizionario (key: edge) (value: linea dell'arco)
#succ_nodes: dizionario (key:nodo) (value: dizionario degli archi disponbili)

#descrizione: tra ogni coppia (current_node. figlio di current_node) calcolo l'arco migliore, 
#ovvero quello con tempo di attesa + tempo di percorrenza minore
#restituisce un dizionario:
#key: tripla(source, dest, chiave dell'arco migliore)
#value: tempo in secondi per percorrerlo
#start_time = stringToTime(start_time)
def best_edge(start_time, current_node, starts, lines, all_lines, succ_nodes):
	prev_line = lines[current_node]
	time_to_move = 180#tempo per cambiare linea
	start_time_fixed = start_time
	
	subgraph_one_edge = {}#key: id arco migliore (id_source, id_dest, edge_key), value: tempo di attesa + tempo di percorrenza

	for dest in succ_nodes[current_node]:#dest: successore di current_node
		
		best_edge_key = (-1, -1, -1)
		best_edge_value = float('inf')

		min = float('inf')
		for edge_key, edge in succ_nodes[current_node][dest].items():#archi tra current_node e dest, edge_key: id arco
			start_time = start_time_fixed
			
			partenza_time = stringToTime(edge['partenza'])#orario partenza di quell'arco

			current_edge = (current_node, dest, edge_key)
			current_line = all_lines[current_edge]#linea nell'arco corrente

			if partenza_time > -1:#non a piedi
				if prev_line != 'NN' and prev_line != 'PIEDI' and prev_line != current_line:
					start_time = start_time + time_to_move

				wait_time = partenza_time - start_time#tempo che devo aspettare dal mio tempo attuale a quello di partenza
			else:
				wait_time = 0
			
			if wait_time >= 0:#escludo i treni che passano prima della mia ora di arrivo in stazione
				if start_time != start_time_fixed:
					wait_time = wait_time + 180
				spending_time = wait_time + edge['tempo']#tempo di attesa più tempo di percorrenza
				if spending_time < min:
					min = spending_time#prendo l'arco con tempo di percorrenza minore
					best_edge_key = current_edge
					best_edge_value = spending_time
					#best_edge_line = current_line
					if dest in starts.keys():
						if partenza_time == -1:#sono a piedi, devo prendere l'orario della fermata precedente + il tempo che ci metto a piedi
							starts[dest] = timeToString(start_time_fixed + spending_time)
						else:	
							if stringToTime(starts[dest]) > stringToTime(edge['arrivo']):#aggiorno la migliore ora di partenza se è minore di quello già presente
								starts[dest] = edge['arrivo']
					else:#se non c'è quell'ora di partenza la aggiungo(quando non ho ancora visitato quel nodo)
						if partenza_time == -1:#sono a piedi, devo prendere l'orario della fermata precedente
							starts[dest] = timeToString(start_time_fixed + spending_time)
						else:
							starts[dest] = edge['arrivo']#quando non ho ancora visitato dest 

		if best_edge_key != (-1, -1 , -1):#controllo per evitare che il minimo  sia stato settato, cioè quando ho almeno un arco
			subgraph_one_edge[best_edge_key] = best_edge_value
	
	del succ_nodes[current_node]
	return subgraph_one_edge

#Metodo che calcola i cammini minimi partendo da un Grafo G, una sorgente ed un'orario di partenza
#G: grafo
#source: nodo di partenza
#start_time: tempo di partenza
#destination: destinazione, se passata non None ritornerà anche il percorso

def mydijkstra(G, source, start_time, destination):
	all_lines = nx.get_edge_attributes(G, 'line')#preprocessing per avere le linee

	inf = float('inf')
	distances = {v: inf for v in G.nodes}#key: nodo value: tempo migliore da source
	distances[source] = 0#source da source ha distanza 0

	if destination != None:
		previous_vertices = {v: None for v in G.nodes}#predecessori per ogni nodo
		previous_edge_key = {v: None for v in G.nodes}#predecessori per ogni nodo

	starts = {}#key: nodo, value: orario di partenza migliore (formato HH:MM:SS)
	starts[source] = start_time

	start_time = stringToTime(start_time)
	seen = []#lista dei nodi già visitati

	lines = {v: None for v in G.nodes}
	lines[source] = 'NN'
	succ_nodes = {v: G._succ[v] for v in G.nodes} #nodi successori a current_node
	
	min_node = min_not_seen(distances, seen)
	while len(seen) < G.number_of_nodes():#cicla finchè ci sono nodi non visitati
		current_node = min_node[0]#prendo l'id del nodo migliore

		if min_node[1] == inf: break#se è infinito è inutile continuare

		seen.append(current_node)#aggiungo il nodo corrente a quelli visitati

		subgraph = best_edge(start_time, current_node, starts, lines, all_lines, succ_nodes)#elenco degli archi più vantaggiosi tra curret_node e i suoi figli

		for edge, value in subgraph.items():#per ogni arco selezionato verifico se migliora le distanze dalla source
			dest = edge[1]#id nodo destinazione
			distance = value #edge[1]#tempo
			distance += distances[current_node]
			
			if distances[dest] > distance:#aggiorno la distanza se è migliore
				distances[dest] = distance
				lines[dest] = all_lines[edge]
				if destination != None:
					previous_vertices[dest] = current_node
					previous_edge_key[dest] = edge
				


		min_node = min_not_seen(distances, seen)
		if min_node[1] == inf: break
			
		start_time = stringToTime(starts[min_node[0]])
	
	#calcolo percorso se richiesto
	if destination != None:
		path = deque()#percorso
		current_node = destination
		current_edge_key = previous_edge_key[current_node]
		while previous_vertices[current_node] is not None:
			path.appendleft((current_node, current_edge_key))
			current_node = previous_vertices[current_node]
			current_edge_key = previous_edge_key[current_node]
		if path:
			path.appendleft((current_node, current_edge_key))

		return (path, distances)
	else:
		return distances

#cammini minimi da ogni nodo come sorgente a tutti i nodi
def all_mydijkstra_distances(M, start_time, nameFileWrite):
	all_distances = {}
	for node in M.nodes():
		dist = mydijkstra(M, node, start_time, destination = None)
		all_distances[node] = dist
		#print("node: ", node)

	fileWrite = "allPath.txt"
	out_file = open(nameFileWrite,"w")
	out_file.write(str(all_distances))
	out_file.close()
	return all_distances
