import csv
import operator
import time
import random

#trasforma l'orario in stringa in secondi
def stringToTime(string_time):
	string_split = string_time.split(":")
	time = 0
	if string_split[0] != "XX":
		time = int(string_split[0]) * 3600
		time += int(string_split[1]) * 60
		time += int(string_split[2])
	else:
		time = -1
	return time 

#trasforma un numero da secoondi ad ora in stringa
def timeToString(second_time):
	return time.strftime("%H:%M:%S", time.gmtime(second_time))


# Metodo che permette di simulare uno sciopero, in base al tasso di adesione passato(probability) 
# venngono mantenuti come nella realtà le fasce garantite 6 e 9 e 18 alle 21
# i treni rimasti illesi dallo sciopero vengono scritti su un file csv che viene ritornato
def strikeGenerator(probability):
	fileWrite = "trainNotSrtikeWith" + str(int(probability * 100)) + "percent" + ".csv"
	out_file = open(fileWrite,"w")
	readTrains = {}
	possibleTrainsIDStrikes = []
	
	#vengono letti tutti i treni, solo quelli che non nelle fasce garantite possono essere influenzati
	c = 0
	with open("edges_add_edge.csv") as csv_file:
		csv_reader = csv.reader(csv_file, delimiter = ",")
		next(csv_reader)
		for row in csv_reader:
			if row[8] not in possibleTrainsIDStrikes:
				if stringToTime(row[3]) > stringToTime("09:00:00") or  stringToTime(row[3]) < stringToTime("06:00:00"):
					if stringToTime(row[4]) > stringToTime("09:00:00") or  stringToTime(row[4]) < stringToTime("06:00:00"):
						if stringToTime(row[3]) > stringToTime("21:00:00") or stringToTime(row[3]) < stringToTime("18:00:00"):
							if stringToTime(row[4]) > stringToTime("21:00:00") or  stringToTime(row[4]) < stringToTime("18:00:00"):
								possibleTrainsIDStrikes.append(row[8])
							
			s = row[0] + "," + row[1] + "," + row[2] + "," + row[3] + "," +row[4] + "," +row[5] + "," +row[6] + "," +row[7] + "," + row[8] + "\n"
			readTrains[c] = (row[8], s)
			c = c + 1

	print('possibili', len(possibleTrainsIDStrikes))			
	out_file.write("#node_1,node_2,line,arrivo,partenza,time,distance(km),weight,trainID\n")
	
	#vengono estratti i treni da cancellare, vengono ritornati
	trains = strikeDecision(possibleTrainsIDStrikes, probability)
	
	print('rimasti', len(trains))
	
	#i treni rimasti vengono riscritti sul file  
	k =0
	for key, trainStop in readTrains.items():
		if readTrains[key][0] in trains:
			out_file.write(readTrains[key][1])
			k = k +1
	
	out_file.close()
	return fileWrite


#metodo che decide quali treni devo essere mantenuti in base al tasso di adesione
#ed all'estrazione di un numero casuale
def strikeDecision(allIDTrains, probability):
	trainsNotStrike = []
	cancelled = 0
	for train in allIDTrains:
		if random.random() > probability:
			trainsNotStrike.append(train)
		else:
			cancelled = cancelled + 1
	return trainsNotStrike
			
#Metodo che permette di ritardare alcuni treni viene passato in input una determinata ora, 
#una distribuzione di probabilità associata al numero di minuti da applicare 
#ritorna un file csv con gli archi dei treni ritardati modificati
def trainsDelayGenerator(hour, distr, mins):
	#extract possibile trains
	allpossibleTrains = []
	with open("edges_add_edge.csv") as csv_file:
		csv_reader = csv.reader(csv_file, delimiter = ",")
		next(csv_reader)
		for row in csv_reader:
			if row[8] not in allpossibleTrains:
				if stringToTime(row[3]) >= stringToTime(hour) and stringToTime(row[3]) <= stringToTime(hour) + 3*3600:
					allpossibleTrains.append(row[8])
	
	#scelta dei treni da ritardare
	dictTrainTodelay = delayTrainDecision(allpossibleTrains, distr, mins)
	
	fileWrite = "trainsRitarted.csv"
	out_file = open(fileWrite,"w")
	
	#scrittura su file 
	with open("edges_add_edge.csv") as csv_file:
		csv_reader = csv.reader(csv_file, delimiter = ",")
		for row in csv_reader:
			if row[8] in dictTrainTodelay.keys():
				row[3] = timeToString(stringToTime(row[3]) + dictTrainTodelay[row[8]] * 60)
				row[4] = timeToString(stringToTime(row[4]) + dictTrainTodelay[row[8]] * 60)
			s = row[0] + "," + row[1] + "," + row[2] + "," +row[3] + "," +row[4] + "," +row[5] + "," +row[6] + "," +row[7] + "," + row[8] + "\n"
			out_file.write(s)
		
	out_file.close()	
	return fileWrite	

#Metodo che permette di eliminare una stazione e i relativi archi che passano per essa
#ritorna il file csv degli archi rimuovnedo quelli passanti per la stazione elminata
def delateStation(stationID):
	fileWrite = "edge_less_" + stationID + ".csv"
	out_file = open(fileWrite,"w")
	with open("edges_add_edge.csv") as csv_file:
		csv_reader = csv.reader(csv_file, delimiter = ",")
		for row in csv_reader:
			if row[0] != stationID and row[1] != stationID:			
				s = row[0] + "," + row[1] + "," + row[2] + "," +row[3] + "," +row[4] + "," +row[5] + "," +row[6] + "," +row[7] + "," + row[8] + "\n"
				out_file.write(s)
		
	out_file.close()
	return fileWrite
	
#Metodo che permette di ritardare tutti i treni che passano in una determinata stazione(station) in un determinato orario(hour)
#leggendo gli archi da un file passato in input. Se vengono passati nell'attributo trainsDelayed gli id dei treni, tali treni
#non subiranno il ritardo.
#Ritorna il file con gli archi modificati
def railwayStationDelayGenerator(station, hour, min, filename = "edges_add_edge.csv", trainsDelayed = []):
	fileWrite = "station" + station +"Delay.csv"
	out_file = open(fileWrite,"w")
	
	nodes = {}
	with open('nodes.csv') as csv_file:
		cvs_reader = csv.reader(csv_file, delimiter = ',')
		next(cvs_reader)
		for row in cvs_reader:
			nodes[row[1]] = row[0]
	stationID = nodes[station]
	
	trains = {}
	with open(filename) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter = ",")
		for row in csv_reader:
			if row[0] == stationID and stringToTime(row[3]) >= stringToTime(hour) and stringToTime(row[3]) <= stringToTime(hour) + 3600:
				ora = stringToTime(row[3])
				trains[row[8]] = ora 
	
	trainsDelayedToReturn = []
	with open(filename) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter = ",")
		for row in csv_reader:
			if row[8] in trains.keys() and row[8] not in trainsDelayed and stringToTime(row[3]) >= trains[row[8]]:
				row[3] = timeToString(stringToTime(row[3]) + min * 60)
				row[4] = timeToString(stringToTime(row[4]) + min * 60)
				trainsDelayedToReturn.append(row[8])
			s = row[0] + "," + row[1] + "," + row[2] + "," +row[3] + "," +row[4] + "," +row[5] + "," +row[6] + "," +row[7] + "," + row[8] + "\n"
			out_file.write(s)
	out_file.close()
	
	return fileWrite, trainsDelayedToReturn

#Metodo che passata una linea, orario e minuti di ritardo, applica il ritardo a tutti gli archi di quella linea
#restituisce il file con gli archi modificati
def delayLine(line, hour, min, filename = "edges_add_edge.csv"):
	fileWrite = "line" + line +"Delay.csv"
	out_file = open(fileWrite,"w")
	with open(filename) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter = ",")
		for row in csv_reader:
			if row[2] == line and stringToTime(row[3]) >= stringToTime(hour) and stringToTime(row[3]) <= stringToTime(hour) + 3600:
				row[3] = timeToString(stringToTime(row[3]) + min * 60)
				row[4] = timeToString(stringToTime(row[4]) + min * 60)
		
			s = row[0] + "," + row[1] + "," + row[2] + "," +row[3] + "," +row[4] + "," +row[5] + "," +row[6] + "," +row[7] + "," + row[8] + "\n"
			out_file.write(s)
	out_file.close()
	return fileWrite

#metodo che decide quali treni devo avere un ritardo e quanti minuti
#in base all'estrazione casuale di un numero e alla distribuzione di probabilità distr
def delayTrainDecision(allpossibleTrains, distr, mins):
	trainToDelay = {}
	for train in allpossibleTrains:
		rn = random.random()
		if rn <= distr[0]:
			continue
		else:
			min = mins[getRange(rn, distr)]
			trainToDelay[train] = min
	return trainToDelay
		
#metodo che ritorna il range in cui è il numero casuale in base alla distribuzione distr
def getRange(numRand, distr):
	if numRand <= distr[1]:
		return 1
	else:
		if numRand <= distr[2]:
			return 2
		else:
			if numRand <= distr[3]:
				return 3
			else:
				return 4
			
		
		

