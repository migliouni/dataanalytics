#File che permette di testare tutte le funzionalità riguardanti percorsi minimi, aggiunta di ritardi e scioperi
import mydijkstra as md
import networkx as nx
import csv
import operator
import random
import ast
import time
import generateDelay as gd
import generateDistance as gdistance
import plotStrikes as plt
import math

#grafo variabile globale
M = nx.Graph()

#trasforma l'orario in stringa in secondi
def stringToTime(string_time):
	string_split = string_time.split(":")
	time = 0
	if string_split[0] != "XX":
		time = int(string_split[0]) * 3600
		time += int(string_split[1]) * 60
		time += int(string_split[2])
	else:
		time = -1
	return time 

#trasforma un numero da secoondi ad ora in stringa
def timeToString(second_time):
	return time.strftime("%H:%M:%S", time.gmtime(second_time))

#restituisce il nome del nodo passando l'id e il grafo
def get_name(id, G):
	for node in list(G.nodes(data = True)):
		if node[0] == id: 
			return node[1]['labels']['label']

#restituisce id del nodo passando il nome e il grafo
def get_id(name, G):
	for node in list(G.nodes(data = True)):
		if node[1]['labels']['label'] == name: 
			return node[0]
			
#Permette di creare il grafo e memorizzarlo nellavariabile globale M, leggendo un csv passato in input	
def createGraph(fileName):
	global M
	data = [('line', str), ('partenza', str), ('arrivo', str), ('tempo', int), ('distanza', float), ('weight', float), ('trainID',str)]
	M = nx.read_edgelist(fileName, comments='#', delimiter=',', create_using=nx.MultiDiGraph, data = data)
	dicto = {}
	with open('nodes.csv') as csv_file:
		cvs_reader = csv.reader(csv_file, delimiter = ',')
		next(cvs_reader)
		for row in cvs_reader:
			dicto[row[0]] = {'label' : row[1]}

	M.add_nodes_from(list(dicto.keys()))
	nx.set_node_attributes(M, dicto, 'labels')

#calcola gli shortest path partendo da una sorgente ad una determinata ora verso tutti gli altri nodi, se viene specificata
#la destinazione (destination) viene anche stampato a schermo il percorso, se viene passato il fileName
#verra prima creato il grafo da tale file poi calcolato i cammini minimi
def calculateShortestPath(source, hour, destination=None, fileName = None):
	global M
	if fileName != None:
		createGraph(fileName)
	id_source = get_id(source, M)
	
	if destination != None:
		#stampa del percorso
		id_dest = get_id(destination, M)
		path, times = md.mydijkstra(M, id_source, hour, id_dest)
		all_lines = nx.get_edge_attributes(M, 'line')
		partenze = nx.get_edge_attributes(M, 'arrivo')
		arrivi = nx.get_edge_attributes(M, 'partenza')
		trainID = nx.get_edge_attributes(M, 'trainID')
		c = 0
		for i in range(1, len(path), 1):
			c = i-1
			print(get_name(path[i-1][0], M),path[i][1],arrivi[path[i][1]], partenze[path[i][1]], all_lines[path[i][1]],trainID[path[i][1]], times[path[c][0]])#, path[i][2])
			if i !=1 and (times[path[c][0]] + md.stringToTime("07:00:00")) != md.stringToTime(partenze[path[i-1][1]]):
				print(times[path[c][0]] + md.stringToTime("07:00:00"), )
				print(md.stringToTime(partenze[path[i-1][1]]), partenze[path[i-1][1]])
		print(get_name(path[len(path)-1][0], M), partenze[path[i][1]], all_lines[path[i][1]],trainID[path[i][1]], times[path[i][0]])
		return times
	
	else:
		id_dest = None
		times = md.mydijkstra(M, id_source, hour, id_dest)
		return times

#permette di calcolare gli shortest path di un grafo(passato tramite fileName) da una sorgente ad una determinata ora 
#calcolando la media in ogni zona, le zone se non viene passato l'attributo differentSource le zone sono calcolate 
#utilizzando come centro della rete il nodo source.
#ritorna le medie in ogni zona e il numero di stazioni irraggiungibili
def shortestPathInZone(source, fileName, hour, differentSource = None):
	allMedia = {1:[],2:[],3:[],4:[],5:[]}
	if differentSource is None:
		#calcola le distanze dal nodo centrale
		distancesFromSource = gdistance.distanceFromStation(source,[10,30,50,75])
	else:
		distancesFromSource = gdistance.distanceFromStation(differentSource,[10,30,50,75])

	times = calculateShortestPath(source, hour, fileName = fileName)
	somma = 0
	countNodes = 0
	countInfInRange = {1:0,2:0,3:0,4:0,5:0}
	sumInRange = {1:0,2:0,3:0,4:0,5:0}
	countInRange = {1:0,2:0,3:0,4:0,5:0}
	for key, value in times.items():
		if value != 0:	#non devo confrontare nodo sorgente
			if value == float('inf'):
				#print(key)
				countInfInRange[distancesFromSource[key][1]] = countInfInRange[distancesFromSource[key][1]] + 1
			else:
				somma = somma + value
				countNodesBase = countNodes +1 
				sumInRange[distancesFromSource[key][1]] = sumInRange[distancesFromSource[key][1]] + value
				countInRange[distancesFromSource[key][1]]= countInRange[distancesFromSource[key][1]] + 1
		
	for k in countInRange:
		print("Zona",k,sumInRange[k]/countInRange[k], countInRange[k], countInfInRange[k])
		allMedia[k].append(sumInRange[k]/countInRange[k])
	
	return allMedia, countInfInRange

#Metodo che calcola gli shortesh path partendo dalla sorgente source, in ogni zona prima e dopo l'eliminazioe successiva di stazioni
#passte nella lista nodesToDelete
def differences(source, hour, nodesToDelete, differenteSource=None):
	global M
	fileName = "edges_add_edge.csv"
	DataToWrite = {1:[],2:[],3:[],4:[],5:[]}
	
	createGraph(fileName)
	if differenteSource is None:
		distancesFromSource = gdistance.distanceFromStation(source,[10,30,50,75])
	else:
		distancesFromSource = gdistance.distanceFromStation(differenteSource,[10,30,50,75])
	times = calculateShortestPath(source, hour)

	sommaBase = 0
	countNodesBase = 0
	countInfBase = 0
	
	medieInRangeBase = {1:0,2:0,3:0,4:0,5:0}
	countInRangeBase = {1:0,2:0,3:0,4:0,5:0}
	
	for key, value in times.items():
		if value != 0:	#non devo confrontare nodo sorgente
			if value == float('inf'):
				countInfBase = countInfBase + 1
			else:
				sommaBase = sommaBase + value
				countNodesBase = countNodesBase + 1 
				medieInRangeBase[distancesFromSource[key][1]] = medieInRangeBase[distancesFromSource[key][1]] + value
				countInRangeBase[distancesFromSource[key][1]] = countInRangeBase[distancesFromSource[key][1]] + 1
	
	for k in countInRangeBase:
		print("Zona",k,medieInRangeBase[k]/countInRangeBase[k], countInRangeBase[k])
		DataToWrite[k].append(medieInRangeBase[k]/countInRangeBase[k])
		
	media = sommaBase/countNodesBase
	print(media,countNodesBase, countInfBase)
	
	
	for node in nodesToDelete:
		print("Node deleted",node)
		#eliminazione node
		M.remove_node(get_id(node, M))
		#ricalcolo shortest path
		times2 = calculateShortestPath(source,hour)
		diff = {}
		countInf = 0
		somma = 0
		countNodes  = 0
		medieInRange = {1:0,2:0,3:0,4:0,5:0}
		countInRange = {1:0,2:0,3:0,4:0,5:0}
		for key, value in times2.items():
			if value != 0:	#non devo confrontare nodo sorgente
				if value == float('inf'):
					countInf = countInf + 1
				else:
					diff[key] =  round(((value -times[key] )/times[key])* 100, 4)
					somma = somma + value
					countNodes = countNodes +1 
					medieInRange[distancesFromSource[key][1]] = medieInRange[distancesFromSource[key][1]] + value
					countInRange[distancesFromSource[key][1]] = countInRange[distancesFromSource[key][1]] + 1
					
	
		media = somma/countNodes
		print(media, countNodes, countInf)
		for k in countInRange:
			print("Zona",k,medieInRange[k]/countInRange[k], countInRange[k]) 
			DataToWrite[k].append(medieInRange[k]/countInRange[k])
	
	return DataToWrite
	
#Metodo che simula numberOfIterations giornate di sciopero al variare del tasso di adesione
#calcolando gli shortest path prima e dopo gli scioperi
def strikeEsaminator(source, probability, numberOfIterations, hour, differentSource = None):
	allMedia= {1:[],2:[],3:[],4:[],5:[]}
	mediaInf = {1:[0],2:[0],3:[0],4:[0],5:[0]}
	for i in range(0,numberOfIterations):
		fileName = gd.strikeGenerator(probability)
		if differentSource is not None:
			data, ninf = shortestPathInZone(source, fileName, hour, differentSource)
		else:
			data, ninf = shortestPathInZone(source, fileName, hour)
			
		for key, value in data.items():
			allMedia[key].append(value[0])	
			mediaInf[key][0] = mediaInf[key][0] + ninf[key]
	
	dizio = {1:0,2:0,3:0,4:0,5:0}	
	for i in range(1,6):
		dizio[i] = sum(allMedia[i])/numberOfIterations
		mediaInf[i][0] = math.ceil(mediaInf[i][0]/ numberOfIterations)
	print(mediaInf)
	return dizio, mediaInf



#Metodo che calcola e visualizza tutti gli shortest path al variare dell'orario di partenza
def testTrainAllHour():
	source = "MILANO PORTA VENEZIA"
	hour = "04:00:00"
	dizio = {}
	dizioInf = {}
	for i in range(5,24):
		hour = timeToString(stringToTime(hour) + 3600)
		allResults, ninf = shortestPathInZone(source, "edges_add_edge.csv", hour)
		dizio[hour] = []
		dizioInf[hour] = []
		for key in allResults:
			dizio[hour].append(allResults[key][0])
			dizioInf[hour].append(ninf[key])
	
	f = open("ANALISIALLHOURS.txt", "w")
	tupla = (dizio, dizioInf)
	f.write(str(tupla))
	f.close()
	plt.showTrainsAllHours()
	

#Esempio di utilizzo del generatoredi sciopero
def testStrike():
	source = "MILANO PORTA VENEZIA"
	probability = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
	hour = "16:00:00"
	allinf = {1:[],2:[],3:[],4:[],5:[]}
	allResults, ninf = shortestPathInZone(source, "edges_add_edge.csv", hour)
	for key,value in ninf.items():
		allinf[key].append(value)
	for prob in probability:
		dizio, medinf = strikeEsaminator(source, prob, 30, hour)
		allResults[1].append(dizio[1])
		allResults[2].append(dizio[2])
		allResults[3].append(dizio[3])
		allResults[4].append(dizio[4])
		allResults[5].append(dizio[5])
		allinf[1].append(medinf[1][0])
		allinf[2].append(medinf[2][0])
		allinf[3].append(medinf[3][0])
		allinf[4].append(medinf[4][0])
		allinf[5].append(medinf[5][0])
		print(allinf)
	f = open("ANALISIStrike.txt", "w")
	tupla = (allResults, allinf)
	f.write(str(tupla))
	f.close()
	plt.showStrikesData()

#DIFFERENZE NEI PERCORSI OTTIMI ELIMINANDO una alla volta le stazioni
def testRemoveStations():
	differentSource = "MILANO PORTA VENEZIA"
	source = "NOVARA"
	hour = "07:00:00"
	#stations =  ["MILANO PORTA GARIBALDI SOTTERRANEA", "MILANO PORTA GARIBALDI","MILANO BOVISA FNM"]#,"MILANO LAMBRATE", "MILANO ROGOREDO"]
	stations = ['TREVIGLIO','MILANO CENTRALE', 'MONZA']
	data = differences(source, hour, stations, differentSource)
	stations.insert(0, "Normal")
	tupla = (data, stations)
	f = open("ANALISIDeleteStations.txt", "w")
	f.write(str(tupla))
	f.close()
	plt.showDeleteStationsData()


def testRemoveInZone():
	source = "MILANO PORTA VENEZIA"
	stations = ['MILANO BOVISA FNM', 'LECCO', 'CODOGNO', 'BRESCIA', 'PIADENA']
	hour = "07:00:00"
	
	for station in stations:
		print(station)
		data, ninf = shortestPathInZone(source, "edges_add_edge.csv", hour)
		
		fileName = gd.delateStation(get_id(station, M))
		dataPost, ninf = shortestPathInZone(source, fileName, hour)
	
		for key, value in dataPost.items():
			data[key].append(value[0])
		print(data, ninf)
		f = open("prova.txt", "w")
		tupla = (data, station)
		f.write(str(tupla))
		f.close()
		plt.showRemoveStationData()


def testDelayCongestione():
	#source = "VARESE"
	differentSource = "MILANO PORTA VENEZIA"
	source = "NOVARA"
	#stations = ['MILANO LAMBRATE', 'TREVIGLIO', 'MILANO CENTRALE', 'MONZA']#, 'MILANO PORTA GARIBALDI SOTTERRANEA']#,'MILANO PORTA GARIBALDI']
	#stations = ['MILANO LAMBRATE', 'MILANO CENTRALE', 'MONZA', 'MILANO ROGOREDO']
	#stations = ['MILANO BOVISA FNM', 'MILANO LANCETTI','MILANO PORTA GARIBALDI']
	stations = ['MILANO BOVISA FNM','MILANO LANCETTI','MILANO PORTA GARIBALDI SOTTERRANEA', 'MILANO REPUBBLICA']
	#stations = ['MILANO LAMBRATE', 'MILANO CENTRALE', 'MONZA', 'MILANO ROGOREDO']
	hour = "07:00:00"
	hourDelay = "08:00:00"
	min = 10
	fileName = "edges_add_edge.csv"
	data, ninf = shortestPathInZone(source, fileName , hour, differentSource)
	trainsDelayed = []
	for station in stations:
		fileName, trains = gd.railwayStationDelayGenerator(station, hourDelay , min, fileName, trainsDelayed)
		#print("nel giro",trains)
		trainsDelayed.extend(trains)
		#print("ritardato totale",trainsDelayed)
		dataPost, ninf = shortestPathInZone(source, fileName, hour, differentSource)
		if stations.index(station) == len(stations) -1:
			for key, value in dataPost.items():
				data[key].append(value[0])
		print(data, ninf)
	name = "BEET10N.txt"
	f = open(name, "w")
	tupla = (data, station)
	print(tupla)
	f.write(str(tupla))
	f.close()
	plt.showDelayCongestione(name)

def testDelayCongestioneLinea():
	source = "LECCO"
	differentSource = "MILANO PORTA VENEZIA"
	line = "S7"
	hour = "07:00:00"
	hourDelay = "07:00:00"
	min = 20
	fileName = "edges_add_edge.csv"
	data, ninf = shortestPathInZone(source, fileName , hour, differentSource)
	fileName = gd.delayLine(line, hourDelay, min)
	dataPost, ninf = shortestPathInZone(source, fileName, hour, differentSource)
	for key, value in dataPost.items():
		data[key].append(value[0])
	name = "ritardoCongestioneS1.txt"
	f = open(name, "w")
	tupla = (data, line)
	f.write(str(tupla))
	f.close()
	plt.showDelayCongestione(name)

#test dello sciopero dalla perifieria verso il centro
def testPeriferia():
	source = "NOVARA"
	differentSource = "MILANO PORTA VENEZIA"
	probability = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
	hour = "8:00:00"
	allinf = {1:[],2:[],3:[],4:[],5:[]}
	allResults, ninf = shortestPathInZone(source, "edges_add_edge.csv", hour,differentSource)
	for key,value in ninf.items():
		allinf[key].append(value)
	for prob in probability:
		dizio, medinf = strikeEsaminator(source, prob, 30, hour, differentSource)
		allResults[1].append(dizio[1])
		allResults[2].append(dizio[2])
		allResults[3].append(dizio[3])
		allResults[4].append(dizio[4])
		allResults[5].append(dizio[5])
		allinf[1].append(medinf[1][0])
		allinf[2].append(medinf[2][0])
		allinf[3].append(medinf[3][0])
		allinf[4].append(medinf[4][0])
		allinf[5].append(medinf[5][0])
		print(allinf)
	f = open("ANALISIStrikepere.txt", "w")
	tupla = (allResults, allinf)
	f.write(str(tupla))
	f.close()
	plt.showStrikesData()


def testRitardi():
	source = "NOVARA"
	differentSource = "MILANO PORTA VENEZIA"
	#source = "MILANO PORTA VENEZIA"
	hour = "07:00:00"
	
	distr = [0.30,0.55,0.75,0.90,1]
	mins = [0,5,10,15,25]
	
	alldays = {}
	sumInZone = {1:0,2:0,3:0,4:0,5:0}
	fileName = "edges_add_edge.csv"
	data, ninf = shortestPathInZone(source, fileName , hour,differentSource)
	for day in range(0,30):
		print("day",day)
		fileName = gd.trainsDelayGenerator(hour, distr, mins)
		dataPost, ninf = shortestPathInZone(source, fileName, hour, differentSource)
		for key, value in dataPost.items():
			data[key].append(value[0])
			sumInZone[key] = sumInZone[key] + value[0]
		name = "test.txt"
		f = open(name, "w")
		tupla = (data, "kkk")
		f.write(str(tupla))
		f.close()
		#plt.showDelayCongestione(name)
		alldays[day] = data
	for key in sumInZone:
		sumInZone[key] = sumInZone[key]/30
	print(sumInZone)
	
'''Richimare una delle funzioni per fare un test'''	
if __name__ == '__main__':
	testTrainAllHour()
	testRemoveStations()
	testStrike()
	testRemoveInZone()
	testPeriferia()
	testDelayCongestione()
	testDelayCongestioneLinea()
	testRitardi()
	
	
